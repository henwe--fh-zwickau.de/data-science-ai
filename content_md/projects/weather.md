---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:weather)=
# Weather

We will work through several projects related to weather data and forecasting. Data will be obtained from [Deutscher Wetterdienst](https://www.dwd.de).

```{tableofcontents}
```
