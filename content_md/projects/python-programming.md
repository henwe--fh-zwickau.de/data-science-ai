---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Python Programming

Programming is like riding a bike. If you want to learn it, you have to do it. Although nowadays there's code available for almost every standard task, implementing simple algorithms like searching and sorting ourselves is very instructive.

```{tableofcontents}
```
