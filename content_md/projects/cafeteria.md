---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:cafeteria)=
# Cafeteria

Have a look at the [menu of Zwickau University's 'Mensa Ring'](https://mobile.fh-zwickau.de/mensa). In this project we want to scrape as much as possible historic menu data from that website. Read [](python:accessing-data) before you start. Section [](python:accessing-data:web-access) is of particular importance.

## The API

Often web APIs come with some documentation. In our case we neither see an API nor documentation. Scraping the current week's menu is simple: get the HTML file and extract data of interest. But how to get the menu for past weeks?

To get access to data of past weeks some research is in order. This part of web scraping is very different for each project and requires some experience in web technologies. For this cafeteria project things work as follows:
* The website offers a button to switch weeks. This button is not a simple link, but performs some more complex action via [JavaScript](https://en.wikipedia.org/wiki/JavaScript), a common browser-based scripting language for websites.
* Checking the website's HTML code we see, that there is an external file [`mensa.js`](https://mobile.fh-zwickau.de/mensa/mensa.js).
* A closer look at `mensa.js` reveals, that there is another file [`get_swcz_data.php`](https://mobile.fh-zwickau.de/mensa/get_swcz_data.php) providing a web API. It's a [PHP](https://en.wikipedia.org/wiki/PHP) script, that is, everytime we request this file from the server the server executes a program and sends the result to our browser.

From `mensa.js` we may guess the arguments accepted by `get_swcz_data.php`:
* `tag` takes values of the form `weekday_mensa` with `weekday` being one of `montag`, `dienstag`, `mittwoch`, `donnerstag`, `freitag` and `mensa` being the cafeteria's unique ID. 'Mensa Ring' has ID 4, 'Mensa Scheffelberg' has ID 3 as we may see from `mensa.js`.
* `week` is the week number (0...52).

Seems that there is no chance to get menus older than a year.

**Task:** Try the API from your browser's address bar. Is the server's answer a valid HTML file?

**Solution:**

```{code-cell} ipython3
# your answer
```

Note that there is another API for accessing cafeteria's menu: [SWCZ Bilderspeiseplan](https://www.swcz.de/bilderspeiseplan). That API allows for specifying year, month and day. But only requests for current and previous week are answered correctly. Having a closer look at that website we see, that there are two more mensa IDs which might work with the API considered above: 1479835489 (Chemnitz, Reichenhainer Straße), 773823070 (Chemnitz, Straße der Nationen).

## Legal Considerations

The second menu source has [license information](https://www.swcz.de/bilderspeiseplan/lizenz.php). There we read that it's okay to use the data for our intended purposes. By the way we see that there is also an API answering with well structured XML instead of HTML. Testing reveals that only data for two weeks is available that way.

Remember to not fire too many requests in short time to the server! This may trigger some protection mechanism making the server refuse any communication with us.
* Limit the number of requests per second by pausing your script after each request.
* While developing and testing automatic download limit the total number of requests to a hand full until you're certain that your script works correctly.

## Getting Raw Data

We proceed in two steps:
* get all the HTML files,
* parse all HTML files.

Parsing will require lots of trial and error. Thus, first downloading all files and parsing in a second step avoids repeated requests to the server while developing and testing code for parsing.

**Task:** Write a Python script which downloads menu HTML files for all weeks, all week days and mensa IDs 3 and 4. Write all files into the same directory. Before you start: How many requests will be send to the server? How long will it take if we send two requests per second?

**Solution:**

```{code-cell} ipython3
# your solution
```

## Parsing

**Task:** From all the downloaded files extract all meals including date, category, description, and prices for students, staff, guests. Save the data to a CSV file.

**Solution:**

```{code-cell} ipython3
# your solution
```
