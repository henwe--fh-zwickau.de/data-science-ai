---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:python:geometric-objects)=
# Geometric Objects

To get a better feeling for the object-oriented approach to programming we implement classes for geometric objects, like roughly sketched in [](python:crash:everything-object). How to draw lines can be guessed from [](python:crash:library-code). Generally, you should have completed the [](python:crash) chapter before starting with this project.

We'll need Matplotlib for this project. So we should import it. In principle, imports can be placed anywhere in the code, but it's considered good practice to have them in the first lines of a file.

```{code-cell} ipython3
import matplotlib.pyplot as plt
```

## Points
 
**Task:** Create a class `Point` with two member variables `x` and `y`. To create a `Point` object we want to call `Point(3, 4)`, for instance.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Triangles

**Task:** Create a class `Triangle`. For creating a `Triangle` object we want to pass three `Point` objects.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Add a `draw` member function to `Triangle` which draws the triangle on screen using Matplotlib. Keep the whole class definition in one code cell, because class definitions cannot be scattered over multiple cells.

**Task:** Create four points and draw two triangles between them (two points are used by both triangles). Remember to call `plt.show()` to show the plot on screen. Without `plt.show()` the plot will show up, too, due to some Jupyter magic. In plain Python you won't see the plot.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Rectangles

**Task:** Create a class `Rectangle` representing an paraxial rectangle. For creating a `Rectangle` object we want to pass two `Point` objects, the lower left corner and the upper right corner. Each of the rectangle's four points should be accessible as a member variable.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Add a `draw` member function to `Rectangle` (use same code cell as before).

**Task:** Draw a series of 20 squares centered at the origin and growing from edge length 2 to 40. Call `plt.axis('equal')` before `plt.show()` to make the squares look like squares.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Houses

**Task:** Create a class `House` representing a house made of a rectangular body and a triangular roof. The roof has one third of the body's height and is 20 per cent wider than the body. For creation we want to provide the lower left corner as well as width of the body and total height of the house. Add a `draw` method.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Draw three houses.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Bonus: Moving Objects

**Task:** Add a `move` method to `Point` which takes two numbers and moves the point paraxially by the specified amounts. Then add `move` methods to `Triangle`, `Rectangle` and `House`, which call `Point`'s move method.

**Task:** Write a function `row_of_houses` for drawing a row of identical houses. Arguments are width and height of the houses as well as start and end coordinates of the row on the x axis. Create one `House` object inside the function. Draw and move the house in a while loop until the house leaves the specified interval.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Draw a row of houses.

**Solution:**

```{code-cell} ipython3
# your solution
```
