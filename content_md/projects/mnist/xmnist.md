---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:mnist:xmnist)=
# The xMNIST Family of Data Sets

In the project we have a first look at the MNIST data set and related data sets. In subsequent projects we'll use these data sets for training machine learning models.

A major benefit from the project is, that we see how difficult data preparation can be. As we'll learn later on, obtaining unbiased data is extremely important for training machine learning algorithms.

## NIST special database 19

**Task:** Learn about *NIST special data base 19* from
* [NIST Special Database 19, Handprinted Forms and Characters Database](https://www.nist.gov/system/files/documents/srd/nistsd19.pdf) (sections 1 and 2)
* [NIST Special Database 19](https://www.nist.gov/srd/nist-special-database-19)

Answer the following questions:
* Who collected the data?
* Who wrote the characters and digits?
* How many images are in the data set?
* How much disk space is needed?

**Solution:**

```{code-cell} ipython3
# your answers
```

## MNIST

**Task:** Learn about *MNIST data set* from
* [Wikipedia](https://en.wikipedia.org/wiki/MNIST_database)
* [The MNIST database of handwritten digits](http://yann.lecun.com/exdb/mnist/)

Answer the following questions:
* Who collected the data?
* How many images are in the data set?
* How much disk space is needed?
* What's the size of the images?
* What preprocessing steps were done?
* What's the up to now best error rate for digit recognition based on MNIST?

**Solution:**

```{code-cell} ipython3
# your answers
```

## QMNIST

**Task:** Learn about *QMNIST data set* from
* [Cold Case: the Lost MNIST Digits](https://arxiv.org/pdf/1905.10498.pdf)
* [QMNIST](https://github.com/facebookresearch/qmnist)

Answer the following questions:
* Who collected the data?
* What are the conditions for using the data set?
* How many images are in the data set?
* How much disk space is needed?
* What's the size of the images?
* What preprocessing steps were done?
* Is QMNIST a superset of MNIST?

**Solution:**

```{code-cell} ipython3
# your answers
```

**Task:** Download the QMNIST data set.

## EMNIST

**Task:** Learn about *EMNIST data set* from
* [EMNIST: an extension of MNIST to handwritten letters](https://arxiv.org/pdf/1702.05373.pdf) (section I and subsections A, B, C of section II)
* [The EMNIST Dataset](https://www.nist.gov/itl/products-and-services/emnist-dataset)

Answer the following questions:
* Who collected the data?
* How many images are in the data set?
* How much disk space is needed?
* What's the size of the images?
* What preprocessing steps were done?
* Why EMNIST was created?

**Solution:**

```{code-cell} ipython3
# your answers
```
