---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:install:python-without-jupyter)=
# Python Without Jupyter

Jupyter is only one of many Python *IDEs* (*integrated development environments*). Although Jupyter is well suited for data science applications, because text, visualizations and code can be mixed in one and the same documents, other tools may be appropriate, too.

## Interactive Python Without IDE

Python (better: the Python interpreter) is a stand-alone program for running Python code on the command line. It has an interactive mode, which executes each line of code immediately after writing. Alternatively, we may provide a file containing Python code and the Python interpreter executes the file's content.

**Task:** Open a terminal and type `python` (don't forget to activate the correct environment with Conda or Anaconda Navigator).

Now the Python interpreter runs in interactive mode. All Python commands are allowed. It's, for instance, a powerful replacement for a calculator.

**Task:** Type the following line by line
```python
1 + 2 * 3
a = 2
b = 3
(a + b) ** 3
```

The result of each line is printed on screen immediately after hitting the return key.

To quit the interpreter we have to call the Python function for leaving a program.

**Task:** Type `exit()`.

## Text Editor Plus Python Interpreter

We may write Python code to a text file and hand it over to the Python interpreter for execution.

There lots of text editors with additional features for coding, like syntax highlighting, automatic indentation, line numbers. Two common ones are [Kate](https://kate-editor.org/) (Linux, MacOS, Windows) and [Notepad++](https://notepad-plus-plus.org) (Windows). Others you may hear about are [Emacs](https://www.gnu.org/software/emacs/), [Vim](https://www.vim.org/) and [Nano](https://www.nano-editor.org/), especially if you work on non-Windows machines (cloud!).

```{figure} xkcd378.png
---
alt: line drawing showing discussion of several people on what's the best editor for programming
---
Real programmers set the universal constants at the start such that the universe evolves to contain the disk with the data they want. Source: Randall Munroe, [xkcd.com/378](https://xkcd.com/378)
```

**Task:** Create a text file `bye.py` with following content:
```python
code = None

while code != "bye":

    print("I'm a Python program. Type 'bye' to stop me:")
    code = input()    # get some input from user

    if code == "":
        print("To lazy to type?")
    print("")

print("Bye")
```

Indentation matters! Python uses indentations (white space) to structure code. So don't modify indentation depth here.

**Task:** Open a terminal, go to your code file's directory and run `python bye.py`.

The Python interpreter now runs your program. When reaching the end of the file, execution stops. Only output from your program is shown. The Python interpreter itself doesn't print anything as long as there are no errors in your program.

## Spyder

[Spyder](https://www.spyder-ide.org/) is a Python IDE for scientific programming with look and feel similar to Octave and Matlab. To use Spyder install the `spyder` package with Anaconda Navigator or via `conda install spyder` in a terminal.

Next to text editor, interactive Python interpreter and separate plotting area Spyder provides tools for debugging (find errors in programs) and for code profiling (measure execution time and memory consumption).

**Task:** Run Spyder (click button in Anaconda Navigator or type `spyder` in terminal).

**Task:** Run the Spyder tour (Help > Show Tour).

**Task:** Open `bye.py` in Spyder. Run the program by clicking the 'run file' button in the toolbar (triangle symbol).

```{figure} spyder.png
---
alt: screenshot of Spyder
figclass: bordered
---
Spyder after running `bye.py`. The variable inspector shows that now there is a variable `code` set in the interactive interpreter.
```

**Task:** Use Spyder's interactive Python console for some computations.

## Executables

Python code can be bundled together with the interpreter and all required libraries into one executable file. This is not recommended because this file will be relatively large, but it's the only way to provide Python programs to people which have not installed a Python interpreter on their machine.

There are several tools for this job. One is known as PyInstaller and ships with the Anaconda distribution. To convert your Python source code file into an executable file open a terminal and type
```
pyinstaller --onefile filename.py
```
This will create a directory named 'dist' containing the executable.

````{hint}
Install the `pyinstaller` package via Anaconda Navigator or via `conda install pyinstaller` in a terminal.
````

**Task:** Make an executable from
```python
code = None

while code != "bye":

    print("I'm a Python program. Type 'bye' to stop me:")
    code = input()    # get some input from user

    if code == "":
        print("To lazy to type?")
    print("")

print("Bye")
```
and run it.
