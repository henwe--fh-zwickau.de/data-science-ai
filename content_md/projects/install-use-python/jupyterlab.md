---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:install:jupyterlab)=
# Working with JupyterLab

In this project you learn basic usage of [JupyterLab](https://jupyter.org), a web interface for Python programming.

````{note}
JupyterLab is not restricted to Python programming, but supports almost every other programming language.
````

## Start JupyterLab

**Task:** Open a webbrowser and go to some JupyterLab provider. Students of Zwickau University should go to [Gauss](https://gauss.fh-zwickau.de).

````{hint}
You may also run a local JupyterLab instance. See [](projects:install:jupyter) for installation and start.
````

After starting JupyterLab you should see a file manager sidebar on the left and the Launcher tab in the working area on the right.

```{figure} launcher-tab.png
---
alt: screenshot of JupyterLab after start-up
figclass: bordered
---
File manager and launcher tab are shown in JupyterLab's initial view.
```

**Task:** Create a new notebook file by clicking the Python button in the launcher tab's Notebook section.

This creates a file `Untitled.ipynb` in the current directory. To change the file name click 'File' and 'Rename Notebook...' in the top menu.

## Write and Execute Code

The new notebook file shows an empty code cell.

**Task:** Write the following Python code to the code cell.
```python
a = 2
b = 3
print(a + b)
```

To run the code do one of the following:
* Click the 'run selected cells' button in the toolbar (triangle button).
* Press Ctrl + Return on your keyboard (runs code and keeps focus on current cell).
* Press Shift + Return on your keyboard (runs code and goes to next cell).

One and the same cell may be executed many times (with or without modifiying the code).

**Task:** Run your code.

A cell's output is show directly below the cell.

**Task:** Write `print(a)` in a new cell, execute the cell and observe the output.

Each cell knows what happend in other cells.

**Task:** Click into the first cell. Then insert a new cell by pressing ESC, then A on your keyboard (not ESC + A). Write `print(a)`, execute and observe output.

Order of cells in the notebook is not of importance. Whether a cell knows about other cells depends on the order of execution!

**Task:** Save your notebook.

```{figure} write-execute.png
---
alt: screenshot of notebook after completing all tasks above
figclass: bordered
---
Your notebook should look as depicted here if you've completed all tasks above.
```

## Kernels

JupyterLab is the connection between you and Python. The code you write to a code cell is send to Python for execution. JupyterLab watches for outputs of your program and displays them to you. The background Python part is referred to as Python kernel.

The connection between a notebook in JupyterLab and the Python kernel is very loose. You may open a notebook file without running a kernel. Then code execution isn't possible. Or you may have a running Python kernel although you closed your notebook.

**Task:** Close your notebook file's tab. Then click the square symbol in the sidebar.

```{figure} kernel-list.png
---
alt: screenshot of kernel list showing a running kernel
figclass: bordered
---
The square button brings up a list of running kernels and some other information.
```

The Python kernel of our test notebook is still running. Clicking the kernel line reopens the notebook tab. The X button (only shown on hover) shuts the kernel down.

**Task:** Shut down the kernel.

````{hint}
Instead of closing a tab and its kernel separately you may also click 'File' and 'Close and Shutdown Notebook'
````

Reopening a notebook runs a fresh kernel.

**Task:** Switch to the file manager in the sidebar and open your notebook (double click its name).

**Task:** Execute the first (that is, top most) cell.

Python complains about an unknown name. The fresh kernel hasn't seen the `a = 2` line up to now, because we did not execute it. Executing the second cell, then the first again works.

````{important}
Always try to create notebooks which can be executed in the same order as cells appear in the notebook!
````

````{hint}
If your code takes too long to run or if it won't stop for some reason, click 'Kernel' and 'Interrupt Kernel' in the menu. This stops code execution and makes the kernel wait for new code.
````

````{hint}
To test your notebook's behavior after launching a fresh kernel but without reopening the file, click 'Kernel' and 'Restart Kernel...' in the menu.
````

## Cell Types

Up to now we only used code cells. Another important cell type are Markdown cells. Markdown is a markup language for writing formatted text.

````markdown
# Some Heading

## A Subsection

Text without formatting. Here's a [link to nowhere](https://nowhere). And a list:
* first item
* second item

This is **bold** and *italic* text.
````

**Task:** Create a new cell (ESC, then A or B to insert new cell above or below current cell). Switch cell type to 'Markdown', either via dropdown in toolbar or via ESC, then M.

**Task:** Write some Markdown code to the cell and execute the cell. To modify Markdown code double click the cell. Then edit and execute again.

## Terminals

JupyterLab allows to run one or more terminals. A terminal is a text interface to the computers operating system. There you can use operating system features and programs not accessible through JupyterLab. Exampels are copying files are deleting non-empty directories.

````{note}
Almost all remote JupyterLab instance run on Linux machines. So in a terminal you have to use Linux commands, not Windows. Linux, macOS, OpenBSD and many other operating systems share a common set of commands. Only Windows has its own set.
````

**Task:** Open a terminal (click the Terminal button in the launcher tab's Other section). Then type `ls` to get a list of all files in the current directory. Then type `logout` to close the terminal.

The `pwd` command prints the current directory's path. Commands for working with files are `cd` (change directory), `cp` (copy), `mv` (move, rename), `rm` (remove), `mkdir` (create directory), `rmdir` (remove directory). See [Unix Commands](https://en.wikibooks.org/wiki/Guide_to_Unix/Commands) for more commands and usage information.

````{hint}
If you close a terminal tab without typing `logout` the terminal remains active in the background. To reopen it or to shut it down click the 'Running Terminals and Kernels' button in the sidebar.
````

## Quit JupyterLab

To quit JupyterLab click 'File' and 'Log Out'. Closing the browser tab without logging out may cause security problems.

Before leaving JupyterLab shut down all kernels and terminals. This saves resources on the server. Most Jupyter providers (including Gauss at Zwickau University) will shut down inactive kernels and JupyterLab sessions after some hours.

````{note}
It's possible to log out from JupyterLab and have a kernel running. Coming back some hours or days later one can fetch the outputs of long running tasks. Corresponding workflow is described in [](projects:install:long-running).
````
