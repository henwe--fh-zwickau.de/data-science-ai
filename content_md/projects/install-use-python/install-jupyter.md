---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:install:jupyter)=
# Install Jupyter Locally

We want to set up an extensible system for Python development and data science in general, including Jupyter as one component. Here we only install the base system. From time to time we will add tools and Python libraries as needed.

````{hint}
A Python library is a collection of Python code files extending Python's set of commands.
````

## Conda

Before we start, we should become aware of two problems:
* A Python development environment consists of many different tools, because many small tools are more flexible than one monolithic all-in-one tool. In principle, one could install all of them manually. On Windows systems this would be very time-consuming. Other operating systems, which adhere to the small tools approach (Unix-like systems), have a package manager for efficient software installation.
* Different programming tasks could require different tools. Sometimes a tool prevents installation of another tool or of another version of itself. This is especially the case for some Python libraries, because some libraries depend on specific versions of others. Installing an additional library could require updating an existing one, but this in turn could corrupt dependencies of already installed libraries.

To circumvent both problems there exist package manager, which create and manage multiple separate Python environments. So we may have several different sets of tools and libraries in parallel, switching between them whenever appropriate.

A widely used package manager for Python is [Conda](https://conda.io). It's part of the [Anaconda](https://www.anaconda.com) and [Miniconda](https://docs.conda.io/en/latest/miniconda.html) Python distributions. A Python distribution is a collection of tools and libraries for Python development.

Miniconda is a light-weight version of Anaconda with fewer packages pre-installed.

## Install Miniconda and Anaconda Navigator

**Task:** Go to [Miniconda Installer List](https://docs.conda.io/en/latest/miniconda.html#latest-miniconda-installer-links) and download a suitable installer for your system. Then follow the [install instructions](https://conda.io/projects/conda/en/latest/user-guide/install/index.html#regular-installation) for your system.

Conda is a command line tool. If you feel more comfortable with GUI tools, install [Anaconda Navigator](https://docs.anaconda.com/anaconda/navigator/).

**Task:** Open a terminal and run `conda install anaconda-navigator` in it. This installs Anaconda Navigator.

Depending on your system now there should be an entry for Anaconda Navigator in your systems app menu. If not add an entry manually. Anaconda executable should be in `bin` subdirectory of Miniconda's installation directory. On Non-Windows run `which anaconda-navigator` in a terminal to get the path.

````{important}
Before you install any additional tools with Anaconda Navigator or Conda, read on!
````

## Create a Python Environment

At the moment there is only one Python environment on your system, called `base`. Don't install additional packages to this environment. Create a separate environment for each kind of task, for instance, an environment you use to work through projects and exercises in this book.

**Task:** Create a new Python environment `ds-book`. Either run `conda create -n ds-book` in a terminal or go to 'Environments' page in Anaconda Navigator. Then click the plus button and follow the GUI instructions.

To switch between environments use Anaconda Navigator or run `conda activate environment_name` in a terminal.

```{figure} xkcd1987.png
---
alt: complex scheme of a system with multiple parallel Python installations
---
The Python environmental protection agency wants to seal it in a cement chamber, with pictorial messages to future civilizations warning them about the danger of using sudo to install random Python packages.. Source: Randall Munroe, [xkcd.com/1987](https://xkcd.com/1987)
```

## Install JupyterLab

Now that we have a Python environment, it's time to install Jupyter.

**Task:** In Anaconda Navigator set package filtering to 'All'. Then head for 'jupyterlab' in the package list and mark it for install. A click an 'Apply' starts installation. Alternatively, run `conda install jupyterlab` in a terminal (make sure you have activated the correct environment).

```{figure} anaconda-install.png
---
alt: screenshot of Anaconda Navigator with highlighted package filter list
figclass: bordered
---
The filter dropdown provides several filters for the package list.
```

````{note}
Although you selected only one package for install, many more will be installed due to dependencies. JupyterLab requires number of other packages and those packages may require others again. Conda manages such dependencies for us.
````

## Launch JupyterLab

The Home page of Anaconda Navigator shows a launch button for JupyterLab. Make sure you selected the correct environment in the dropdown above the launch buttons.

**Task:** Launch JupyterLab via Anaconda Navigator or from the command line: `jupyterlab`.

Freshly installed JupyterLab lives in the environment you installed it in. Creating a new Python environment requires to install JupyterLab in this environment, too (if you want to use JupyterLab there).

Default behavior of JupyterLab is to show you your home directory and to disallow visiting directories outside your home directory via JupyterLab. To access a different directory, run JupyterLab from a terminal. Then JupyterLab will show the directory active in the terminal when launching JupyterLab.

## Install Python Packages

There's already a basic Python installation in your environment. So you can use Python in JuyterLab. Additional packages (math, visualization,...) can be installed on demand in the same way we installed JupyterLab. Always keep an eye on the environment name when installing. So things will end up in the correct environment.

```{hint}
Next to Conda there exist other package managers. A very prominent one is [Pip](https://pypi.org/project/pip/). Conda automatically installs Pip in each environment. To install a package with Pip simply write `pip install package_name` in a terminal. Conda will take care of packages installed with Pip, too.

Some packages are only available for install with Conda or only for install with Pip. So both package manager have to be used in parallel.
```

## JupyterLab Desktop App

Recently, a [JupyterLab Desktop App](https://github.com/jupyterlab/jupyterlab-desktop) has been released. This brings the look and feel of usual GUI apps to JupyterLab's start-up process. After start-up there's no difference to browser based JupyterLab.

Handling of different Python environments is somewhat more difficult than with plain JupyterLab.
