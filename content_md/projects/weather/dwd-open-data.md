---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:weather:dwd-open-data)=
# DWD Open Data Portal

[Deutscher Wetterdienst (DWD)](https://www.dwd.de) is Germany's public authority for collecting, managing and publishing weather data from around the world. DWD also creates weather forecasts for Germany and all other regions of the world. Some years ago DWD launched an [Open Data Portal](https://www.dwd.de/opendata) and continually extends its services there.

DWD's open data portal provides lots of data and is very complex. In this project we explore part of its structure and locate data sources for subsequent projects.

## Licensing

We want to use DWD's data for education and research. Before we delve into the data we should check whether we are allowed to do this and whether and how to attribute the source.

**Task:** Find out whether we are allowed to use DWD's data for education and research puposes.

**Solution:**

```{code-cell} ipython3
# your answer
```

**Task:** Do we have to refer to DWD as data provider? If yes, how?

**Solution:**

```{code-cell} ipython3
# your answer
```

## Weather Stations

There are lots of weather stations at Germany collecting weather data. To locate wheather data on the map, we need a list of stations and their geolocations.

**Task:** Find a list of all DWD weather stations at Germany measuring air temperature at least once per hour. The list should containing geolocations (longitude, latitude, altitude) and other parameters. Get the URL, so we can download it on demand.

**Solution:**

```{code-cell} ipython3
# your answer
```

**Task:** How many weather stations do we have in the list? List all parameters available for the stations.

**Solution:**

```{code-cell} ipython3
# your answer
```

**Task:** Get the station list for hourly precepitation measurements. How many stations do we have here?

**Solution:**

```{code-cell} ipython3
# your answer
```

## Data

For each weather station we want to have access to all its historical and most recent measurements.

**Task:** Locate hourly temperature measurements for station [Lichtentanne](https://www.openstreetmap.org/#map=16/50.6879/12.4329). What's the most recent measurement (timestamp and temperatur)? What's the oldest measurement available?

**Solution:**

```{code-cell} ipython3
# your answer
```

## Metadata

Each station comes with extensive metadata telling a story about the station and its measurements.

**Task:** Answer the following questions from metadata of Lichtentanne station:
* Did the station move? If yes, when? Was it's name changed, too?
* Has measurement equipment been replaced? If yes, when?
* What's the time zone of timestamps in the data?

**Solution:**

```{code-cell} ipython3
# your answer
```

Whenever you find abnormalities in measurements, first check metadata!
