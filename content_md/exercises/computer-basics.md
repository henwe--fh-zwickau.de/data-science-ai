---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:computer-basics)=
# Computer Basics

To solve these exercises on bits and bytes and representations of numbers you should have read [](warm-up:computers-programming).

## Bits and Bytes

### Hard Disk Capacity

A hard disk is advertised to have a capacity of 2 TB. Give the hard disk's capacity in TiB.

**Solution**

```{code-cell} ipython3
# your solution
```

### Uncompressed Image File

An image has a width of 4000 pixels and a height of 3000 pixels. For each pixel three color components (red, green, blue) have to be saved, where 8 bits per component are required. How much space does the image need when saving it to a file without compression?

**Solution**

```{code-cell} ipython3
# your solution
```

### Books

A book with 500 pages and 1500 characters per page in average shall be saved to a file. Each character requires one byte of disk space. What is the file's total size?

A well known online encyclopedia has about 50 GB of English text (without images). How many books are needed for a print version?

How long does it take to transfer 50 GB of data with a transfer rate of 100 megabit per second?

**Solution**

```{code-cell} ipython3
# your solution
```

### Uncompressed Video File

How much storage is required for an uncompressed 120 minutes video file with 30 frames (that is, images) per second and full HD resolution (1920x1080 pixels, 24 bits per pixel).

What's the compression rate if the video file has size 4.1 GB?

**Solution**

```{code-cell} ipython3
# your solution
```

## Representation of Numbers

### Binary to Decimal

Write the binary numbers as decimal numbers.
* 10000001
* 11010010

**Solution**

```{code-cell} ipython3
# your solution
```

### Decimal to Binary

Write the decimal numbers as binary numbers:
* 15
* 16
* 17
* 123

**Solution**

```{code-cell} ipython3
# your solution
```

### Decimal to Hexadecimal

Write the decimal numbers as hexadecimal numbers:
* 31
* 32
* 33
* 234
* 257

**Solution**

```{code-cell} ipython3
# your solution
```

### Binary to Hexadecimal

Write the 32-digit binary number 10001001 11111110 00001010 01001101 as hexadecimal number.

**Solution**

```{code-cell} ipython3
# your solution
```

### Memory vs. Storage

Give two differences between a computer's memory and a mass storage device.

**Solution**

```{code-cell} ipython3
# your solution
```

### Compilers and Interpreters

What's the difference between compiled and interpreted computer programs?

**Solution**

```{code-cell} ipython3
# your solution
```
