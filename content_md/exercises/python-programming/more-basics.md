---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:more-basics)=
# More Basics

Solving this set of exercises increases your skills in algorithmic thinking and Python's syntax. Everything you need has been discussed in the [](python:crash) chapter. Do not use additional Python features or modules.

## Point Inside Rectangle?

Get two integers from the user and check whether the corresponding point lies inside the rectangle with corners at (-1, -1), (5, -1), (5, 2), (-1, 2). Print a message showing the result.

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your solution
```

## Square Numbers

Get an integer from the user and tell the user whether it's a square number or not. If you want to compute square roots, use `123 ** 0.5`. Print a message if the user gave a negative number.

Hint: Have a look at the output of `16.125 % 1`.

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your solution
```

## Unique Items

Write a function `no_duplicates` which returns `True` if the passed list contains no duplicates and `False` if there are duplicates.

Test your function with `[1, 4, 5, 6, 3]` and `[1, 3, 1]` (and with `[]`, of course).

**Solution:**

```{code-cell} ipython3
# your solution
```

## Increasing Subsequence

Write a function `inc_subseq` which takes a list of numbers and prints all items except the ones which are smaller than their predecessor.

Test your function (at least) with `[1, 3, 2, 3, 4, -2, 9]` and `[3, 2, 1]`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Area of a Circle

Get an integer radius of a circle from the user. Calculate and print the circle's area as well as the edge length of a square with identical area. Check user input for validity. You may use NumPy's `pi` constant.

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your solution
```

## Quadratic Equations

Solve the quadratic equation $a\,x^2+b\,x+c=0$ with user-specified $a$, $b$, $c$ (integers). Give all real solutions.

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your solution
```

## Regular Polygons

Use Matplotlib and NumPy's `sin` and `cos` functions to plot a regular polygon. Ask the user for the number of vertices and check user input for validity.

Hint: For $n$ vertices the $k$th vertex is at $(\cos\varphi,\sin\varphi)$ with $\varphi=2\,\pi\,\frac{k}{n}$.

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your solution
```

## Stars

Draw a star with user-specified number of outside vertices. Radius for inner vertices is 0.3, for outer vertices it's 1.

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your solution
```
