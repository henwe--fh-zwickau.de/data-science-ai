---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:basics)=
# Basics

Before solving these programming exercises you should have read [](python:crash:blocks). Only use Python features discussed there.

## Python as a Calculator 1

If light travels 300 million meters per second, how many kilometers travels light in one hour?

**Solution:**

```{code-cell} ipython3
# your solution
```

## Python as a Calculator 2

A 20 years old person started watching web videos at the age of 8. Everyday the person watches videos for 2 hours. If the person would have watched videos the same total number of hours, but all the day instead of only 2 hours, how many years of its young life would the person have wasted?

Take into account, that the person uses approximately 7 hours for sleeping and 3 ours for eating, grooming, and doing housework. Thus, time for watching videos is less than 24 hours a day.

Assume that each year has 365 days.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Integer Division

Get two integers from the user. Divide the first by the second. Use floor division and show the remainder, too. Avoid `ZeroDivisionError`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Conditional Execution 1

Get three integers from the user and print a message if they are *not* in ascending order.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Conditional Execution 2

Get three integers from the user und print them in ascending order.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Functions 1

Write a function `is_ascending` which checks whether its three numeric arguments are in ascending order. Return a boolean value.

Test the function with an ascending sequence and a non-ascending sequence.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Functions 2

Write a function `cut_off_decimals` which takes two arguments, a float and a positive integer. The function shall cut off all but the specified number of decimal places of the float and then return the float. If the second argument is negative, the float shall remain untouched.

Note, that floor devision also works for floats. Thus, `x // 1` cuts off all decimal places.

Test the function with 1.2345 and 2 decimal places.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Loops 1

Print the numbers 0 to 9 with a while loop. In other words: Use a while loop to simulate a for loop.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Loops 2

Take a list and print its items in reverse order. Test your code with `[-1, 2, -3, 4, -5]`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Loops 3

Take a list and print it item by item. After each item ask the user whether he wants to see the next item. If not, stop printing. If yes, go on.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Loops 4

Take a list and print it item by item. After each item ask the user whether he wants to see the next item. If not, stop printing. If yes, go on. If there are no more items left, start again with the first item.
**Solution:**

```{code-cell} ipython3
# your solution
```
