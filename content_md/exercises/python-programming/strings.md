---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:strings)=
# Strings

Read [](python:strings) before you start with the exercises.

## Character Statistics

Print a list of all characters appearing in a string. Count how often each character appears **without** using `str.count`. Get the string from user input.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Unicode Fruits

Print the string `I like apples and melons.` where `apples` and `melons` shall be replaced by a suitable Unicode symbol.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Parser

Write a function which converts a string representation of a table of integers to a list of lists of integers. The elements of a row are separated by commas and rows are separated by semicolons. Test your function with
```python
'1,2,3;4,5,6;7,8,9'
```
Result should be
```python
[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
```
Bonus: Solve the task in one line.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Spoon Language

Write a Python script which prompts the user for some input and translate user input to [spoon language](https://de.wikipedia.org/wiki/Spielsprache#L%C3%B6ffelsprache).

Hint: Simple replacement does not work. If, for instance, at first *a* is replaced by *alewa*, subsequent replacement of *e* will alter the *lew* in *alewa*. There seems to be no replacement order for vowels working correctly in each case. Thus, in a first run replace all vowels by some code (unlikely to appear in a text), then, in a second run, replace the codes by the vowel's lew-version.

**Solution:**

```{code-cell} ipython3
# your solution
```
