---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:file-access)=
# File Access

Read [](python:accessing-data) before you start with the exercises.

## Lower Case Copy

Read some text file's content, convert it to lower case and save it to a new text file.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Reading CSV Files

Get a CSV file containing all public trees at Chemnitz from the [Open data portal of Chemnitz](http://portal-chemnitz.opendata.arcgis.com). Read the first 10 lines from the file and show them on screen.

Hint: If you encounter cumbersome symbols in the output, have a look at [byte order marks](https://en.wikipedia.org/wiki/Byte_order_mark) at Wikipedia. 

**Solution:**

```{code-cell} ipython3
# your solution
```

## Reading ZIP files

Get 'The Blog Authorship Corpus' from the web. The original source [https://u.cs.biu.ac.il/~koppel/BlogCorpus.htm](https://u.cs.biu.ac.il/~koppel/BlogCorpus.htm) vanished in 2022. Use [https://www.fh-zwickau.de/~jef19jdw/datasets/blogs.zip](https://www.fh-zwickau.de/~jef19jdw/datasets/blogs.zip). The ZIP file contains an `info.txt` file and the original ZIP file.

Read `infos.txt` to get information about the file name format used in the ZIP file.

Write a Python program which extracts all 4 features from the file names and saves them in a CSV file.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Reading XML Files

Open the file `7596.male.26.Internet.Scorpio.xml` from 'The Blog Authorship Corpus' (see exercise above) without extracting it explicitly. Print the first and the last post in the file to screen.

**Solution:**

```{code-cell} ipython3
# your solution
```
