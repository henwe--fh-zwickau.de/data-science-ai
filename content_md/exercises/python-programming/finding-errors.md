---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:finding-errors)=
# Finding Errors

Computer programs contain lots of errors. Finding them is much more difficult than correcting them. In this series of exercises you have to find syntax and semantic errors in small programs. Before you start you should have read [](python:crash:blocks).

```{hint}
Simply run the programs and let the Python interpreter look for errors. Then correct all errors identified by the interpreter. If still something is not working as expected, have a closer look at the source code.
```

## Simple 1

```python
print('Python is a programming language.')
print('Many people love Python's approach to programming.')
print('Maybe it's the first programming language you learn to use.')
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

print('Python is a programming language.')
print('Many people love Python's approach to programming.')
print('Maybe it's the first programming language you learn to use.')
```

## Simple 2

```python
a = 2
b = 3

if a < b
     print(a, '<', b)
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

a = 2
b = 3

if a < b
     print(a, '<', b)
```

## Simple 3

```python
def say_something(text):

    print('I have to say:')
    print(text)
    
say_samething('Python is great!')
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

def say_something(text):

    print('I have to say:')
    print(text)
    
say_samething('Python is great!')
```

## Simple 4

```python
a = 2
b = 3

if a = b:
    print(a, 'equals', b)
else:
    print('not equal')
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

a = 2
b = 3

if a = b:
    print(a, 'equals', b)
else:
    print('not equal')
```

## Slightly More Difficult 1

```python
primes = [2, 3, 5, 7, 11, 13]

print('third prime number is', primes(2))
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

primes = [2, 3, 5, 7, 11, 13]

print('third prime number is', primes(2))
```

## Slightly More Difficult 2

```python
problem = '5 + 4'
solution = '5' + '4'

print(problem, '=', solution)
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

problem = '5 + 4'
solution = '5' + '4'

print(problem, '=', solution)
```

## Slightly More Difficult 3

```python
my_list = [1, 3, 4, 2, 6]

print('last item of my list is', my_list[5])
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

my_list = [1, 3, 4, 2, 6]

print('last item of my list is', my_list[5])
```

## Slightly More Difficult 4

```python
last = 5

print('printing all square numbers up to square of', last)
for k in range(1, last):
    print(k + k)
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

last = 5

print('printing all square numbers up to square of', last)
for k in range(1, last):
    print(k + k)
```

## More Difficult 1

```python
print('Adding zero to 5 doesn't change anything:')
print(5 + O)
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

print('Adding zero to 5 doesn't change anything:')
print(5 + O)
```

## More Difficult 2

```python
l = [2, 3, 2, 5, 7, 8, 9]

for i in range(0, len(l)):
    print('product of item', i, 'and item', i + 1, 'is', l[i] * l[i + 1])
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

l = [2, 3, 2, 5, 7, 8, 9]

for i in range(0, len(l)):
    print('product of item', i, 'and item', i + 1, 'is', l[i] * l[i + 1])
```

## More Difficult 3

```python
l = [2, 4, 6, 3]

print('let\'s print the list', l, 'item by item:')
for i in range(1, len(l)):
    print(l[i - 1])
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

l = [2, 4, 6, 3]

print('let\'s print the list', l, 'item by item:')
for i in range(1, len(l)):
    print(l[i - 1])
```

## Difficult 1

```python
my_lists = [[1, 2, 3], [4, 2, 5, 6, 7], [1, 4], [0]]

total_sum = 0
for i in range(0, len(my_lists)):
    for j in range(0, len(my_lists[0])):
        total_sum = total_sum + my_lists[j][i]
        
print(total_sum)
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

my_lists = [[1, 2, 3], [4, 2, 5, 6, 7], [1, 4], [0]]

total_sum = 0
for i in range(0, len(my_lists)):
    for j in range(0, len(my_lists[0])):
        total_sum = total_sum + my_lists[j][i]
        
print(total_sum)
```

## Difficult 2

```python
last = 19

print('detecting prime numbers below or equal to', last)
for n in range(2, last):
    for k in range(2, n / 2):
        if n % k != 0:
            break
    else:
        print(n)
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

last = 19

print('detecting prime numbers below or equal to', last)
for n in range(2, last):
    for k in range(2, n / 2):
        if n % k != 0:
            break
    else:
        print(n)
```

## Difficult 3

```python
def print_first_half(l):
    '''print first half of list, include center item if length of list is odd'''

    print('first half of', l, 'is')
    for i in range(0, len(l) / 2):
        print(l[i])

        
def print_second_half(l):
    '''print second half of list, omit center item if length of list is odd'''

    print('second half of', l, 'is')
    for i in range(len(l) / 2, len(l)):
        print(l[i])

        
l = [2, 4, 3, 6, 8, 5, 7]
print_first_half(l)
print_second_half(l)

l = [2, 4, 3, 6, 8, 5]
print_first_half(l)
print_second_half(l)

l = [1]
print_first_half(l)
print_second_half(l)

l = []
print_first_half(l)
print_second_half(l)
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

def print_first_half(l):
    '''print first half of list, include center item if length of list is odd'''

    print('first half of', l, 'is')
    for i in range(0, len(l) / 2):
        print(l[i])

        
def print_second_half(l):
    '''print second half of list, omit center item if length of list is odd'''

    print('second half of', l, 'is')
    for i in range(len(l) / 2, len(l)):
        print(l[i])

        
l = [2, 4, 3, 6, 8, 5, 7]
print_first_half(l)
print_second_half(l)

l = [2, 4, 3, 6, 8, 5]
print_first_half(l)
print_second_half(l)

l = [1]
print_first_half(l)
print_second_half(l)

l = []
print_first_half(l)
print_second_half(l)
```
