---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:lists-friends)=
# Lists and Friends

To solve these exercises you yould have read [](python:lists-friends). Only use features discussed there or in previous chapters.

## Bad Coding Style

Consider the following code snipped. What numbers appear on screen when printing `a`, `b`, `e`, `g`, and `h` after executing the code? Don't run the code, interpret each line manually.

```python
a = 1
b = 2
c = [a, b]
c[0] = 3
d = c
d[1] = 4
c.append(5)
e = c[-1]
f = c[0:-1]
g = f[-1]
h = d[0]
```

**Solution:**

```{code-cell} ipython3
# your answer
```

## Squares and Sums of List of Lists

The following code yields incorrect outputs. Find the problem and solve it.

```python
a = [1, 2, 3]
b = [4, 5, 6]
c = [7, 8, 9]
d = [a, b, c]

# compute squares
for i in range(0, 3):
    for j in range(0, 3):
        d[i][j] **= 2
        
# compute row sums
sum_a = a[0] + a[1] + a[2]
sum_b = b[0] + b[1] + b[2]
sum_c = c[0] + c[1] + c[2]

# print results
print('squares of d:', d)
print('sum of a:', sum_a)
print('sum of b:', sum_b)
print('sum of c:', sum_c)
```

**Solution:**

```{code-cell} ipython3
# your modifications

a = [1, 2, 3]
b = [4, 5, 6]
c = [7, 8, 9]
d = [a, b, c]

# compute squares
for i in range(0, 3):
    for j in range(0, 3):
        d[i][j] **= 2
        
# compute row sums
sum_a = a[0] + a[1] + a[2]
sum_b = b[0] + b[1] + b[2]
sum_c = c[0] + c[1] + c[2]

# print results
print('squares of d:', d)
print('sum of a:', sum_a)
print('sum of b:', sum_b)
print('sum of c:', sum_c)
```

## Slicing Instead of Loops 1

Write a function `shift` which takes a list, increases each item's index by (last item becomes first one), and returns the resulting list. Example: `[1, 3, 5, 7]`should become `[7, 1, 3, 5]`. Don't use loops, but slicing syntax.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Slicing Instead of Loops 2

Write a function `shift_n` which takes a list and a positive integer `n` and shifts the list `n` times (cf. previous task). Don't use loops. Do not forget to think about the case that `n` is larger than the length of the list. Examples:
* `shift_n([1, 2, 3, 4, 5], 3)` should be `[3, 4, 5, 1, 2]`.
* `shift_n([1, 2, 3, 4, 5], 5)` should be `[1, 2, 3, 4, 5]`.
* `shift_n([1, 2, 3, 4, 5], 6)` should be `[5, 1, 2, 3, 4]`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Dictionary From Lists 1

Given two lists `keys` and `values` create a dictionary. Use a for loop to fill an empty dictionary item by item. Test case:
```python
keys = ['a', 'b', 'c', 'd']
values = [1, 2, 3, 4]
```

**Solution:**

```{code-cell} ipython3
# your solution
```

## Dictionary From Lists 2

Given two lists `keys` and `values` create a dictionary. Use a dictionary comprehension. Test case:
```python
keys = ['a', 'b', 'c', 'd']
values = [1, 2, 3, 4]
```

**Solution:**

```{code-cell} ipython3
# your solution
```

## Dictionary From Lists 3

Given two lists `keys` and `values` create a dictionary. Call `dict` and pass a list of key-value pairs. Test case:
```python
keys = ['a', 'b', 'c', 'd']
values = [1, 2, 3, 4]
```

**Solution:**

```{code-cell} ipython3
# your solution
```

## One-Liner 1

Given a list of numbers, write one line of code to create a new list containing only numbers greater than 3. Test case:
```python
[4, 3, 2, 8, 6, 0, 4, 6, -2, 1]
```

**Solution:**

```{code-cell} ipython3
# your solution
```

## One-Liner 2

In one line of code create a new list containing every second number from a given list, if the number is between 1 and 10 (both included). Test case:
```python
[-2, 3, 2, 6, 23, 1, 42, 42, 5, 10, 1, 12, 6, 4, 3]
```

**Solution:**

```{code-cell} ipython3
# your solution
```

## One-Liner 3

Write one line of code to square all numbers in a list of lists of numbers. Test case:
```python
[[1, 2, 3], [7, 6, 5, 4], [8, 9]]
```

**Solution:**

```{code-cell} ipython3
# your solution
```

## Manual Deep Copying

Make a copy of a list of lists of numbers. In the end, changing a number in the copy must not modify the original numbers. Test case:
```python
[[1, 2, 3], [7, 6, 5, 4], [8, 9]]
```

**Solution:**

```{code-cell} ipython3
# your solution
```
