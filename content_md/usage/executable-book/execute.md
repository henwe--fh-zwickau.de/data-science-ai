---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(usage:execute)=
# Manipulate and Execute Code

Python code in this book can be executed in different ways without copying the code manually. The HTML rendering's upper right corner shows a rocket symbol. The rocket button provides several options for executing a page's code.

```{figure} launch-button.png
---
alt: screenshot of HTML rendering while hovering over launch button, menu shows options Binder, Gauss, Live Code
figclass: bordered
---
Hovering over the rocket symbol provides several options for code execution.
```

The next section contains some Python code for testing code execution right here on this page. Subsequent sections describe button functionality in more detail. Local code execution on your machine is described, too.

```{attention}
All code execution features but Live Code use the book's [Jupyter](https://jupyter.org) rendering. For technical reasons the Jupyter rendering lacks some figures and text formatting may be incorrect. For reading without a need for code execution stay with the HTML or PDF renderings.
```

## Sample Code for Testing

Here we have some simple Python code for testing code execution features of this executable book. Details on these features are given below.

```{code-cell} ipython3
a = 2
b = 6
print(a, '+', b, '=', a + b)
```

## Launch on Binder

The Binder launch button opens a [JupyterLab](https://jupyter.org) session on [mybinder.org](https://mybinder.org). There you find the book's Jupyter rendering. The Jupyter rendering is a collection of Jupyter Notebooks (files with `ipynb` extension). The Binder launch button opens your current page's Jupyter rendering, but all other pages are available, too, in one and the same Binder session.

```{figure} binder-startup.png
---
alt: screenshot of Binder startup page showing URL and name of the requested repository
figclass: bordered
scale: 70%
---
Binder startup requires cloning the book's Git repository if something has changed since last Binder usage.
```

Starting the Binder session may take some seconds. Keep in mind that [mybinder.org](https://mybinder.org) is a free service provided by volunteers and supported by donators. Don't overuse it to keep it free and available to everybody. Don't run complex computations like neural network training on Binder.

The JupyterLab session on Binder allows for code editing and repeated execution. You may also save your files there, but they will be lost as soon as you end the session. Don't forget to download modified files to your local machine before you leave.

## Launch on Gauss

[Gauss](https://gauss.fh-zwickau.de) is a GPU server at Zwickau University of Applied Sciences **only available in the university's intranet**. Students with access to Gauss should use Gauss instead of Binder. The Gauss launch button runs the book's Jupyter rendering in JupyterLab on Gauss very similar to Binder.

```{figure} gauss-login.png
---
alt: screenshot of Gauss login page asking for username and password
figclass: bordered
scale: 70%
---
Gauss asks for username and password before launching a book's page in JupyterLab.
```

A click on the Gauss launch button copies the whole [GitLab repository](https://gitlab.hrz.tu-chemnitz.de/jef19jdw--fh-zwickau.de/data-science-ai) of the book to the user's personal directory on Gauss. Thus, modifications to code and other files are saved to the user's directory, too, and are persistent.
Repeated clicks on the Gauss launch button do not overwrite a user's modifications, but may update files untouched by the user but modified in the GitLab repository. Thus, the user's version will always be up-to-date while preserving the user's modifications as far as possible. For details on the merge process run when clicking the Gauss launch button see [Automatic Merging Behavior](https://jupyterhub.github.io/nbgitpuller/topic/automatic-merging.html) in nbgitpuller's documentation.

## Live Code

The Live Code button makes code cells editable and executable on-the-spot using [Thebe](https://github.com/executablebooks/thebe). Clicking the Live Code button starts a Python kernel on [mybinder.org](https://mybinder.org) and connects the book's HTML rendering with that kernel. Progress and success of the startup process are shown below the page's heading.

```{figure} launching-thebe.png
---
alt: screenshot of HTML rendering after lauching Live Code, the info box says that the Python kernel is ready
figclass: bordered
---
A box with progress information appears after clicking the Live Code button.
```

Code cells on the page change their appearance. Outputs now belong to the cell, some buttons appear, and the code becomes editable.

```{figure} thebe-cell.png
---
alt: screenshot of HTML rendering after lauching Live Code, the code cell now has buttons for controlling code execution
figclass: bordered
---
After lauching Live Code each code cell shows buttons for starting and controlling code execution.
```

Cells are not run immediately after clicking the Live Code button. Clicking the 'run' button executes the cell. Alternatively, one may run all cells on a page by clicking the 'restart & run all' button.

## Local Code Execution

To execute the book's Python code on your local machine download a pages Jupyter rendering by clicking the download button in the upper right corner of the HTML rendering or clone the book's [Git repository](https://gitlab.hrz.tu-chemnitz.de/jef19jdw--fh-zwickau.de/data-science-ai) to your machine.

```{figure} download-button.png
---
alt: screenshot of HTML rendering after hovering the download button, download options ipynb and pdf are shown
figclass: bordered
---
Hovering the download symbol shows a list of available formats.
```

On your machine you need [JupyterLab](https://jupyter.org) or a similar tool from the Jupyter ecosystem to view and modify the `ipynb` files. For install instructions have a look at the [](projects:install:jupyter) project.
