---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Read Online or Offline

This book has been created with [Jupyter Book](https://jupyterbook.org). It comes in different formats and variants. The reader may choose according to her or his preferences. The reader may even switch between different variants at will.

## Online in a Webbrowser

The intended medium for reading this book is a website in a webbrowser, that is, the [HTML rendering of the book](https://www.fh-zwickau.de/~jef19jdw/data-science-ai). There you have full functionality including interactive features and live code editing and execution, see [](usage:execute) for details.

The HTML rendering come with an optional fullscreen mode (button in the upper right corner). You may also hide the table of contents sidebar on small screens (arrow button in the upper left corner).

```{figure} buttons.png
---
alt: screenshot of HTML rendering with highlighted fullscreen and sidebar button
figclass: bordered
---
Fullscreen button and sidebar button allow to adjust the book's layout to your screen.
```

## Offline in a Webbrowser

You may download the whole HTML rendering as ZIP archive. After extracting the archive open the file `index.html` in a webbrowser. All features will work like in the online version, but some content, like externally hosted videos, won't be available without Internet connection.

The HTML rendering is a static website, that is, no webserver is needed for reading. Everything works on your local machine.

````{card}
:link: https://www.fh-zwickau.de/~jef19jdw/data-science-ai/data-science-ai.zip
:link-type: url
:text-align: center
**Download HTML rendering in ZIP file**
````

## Offline PDF ebook

For printing and for friends of higher quality typesetting there is a PDF version of the book. Of course, the PDF version lacks interactive features and in-place code editing and execution.

````{card}
:link: https://www.fh-zwickau.de/~jef19jdw/data-science-ai/data-science-ai.pdf
:link-type: url
:text-align: center
**Download PDF ebook**
````
