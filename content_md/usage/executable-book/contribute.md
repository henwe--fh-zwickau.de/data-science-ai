---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Contribute

This book is not static. It will grow over time and existing material will be rearranged, updated, reduced or extended as required by future developments in data science, AI and teaching. In this sence, it's a living or dynamic book. And you, the reader, are invited to contribute to the book.

The book's HTML rendering shows a contribution button in the upper right corner offering several options. Those options will be discussed in detail below.

```{figure} repo-button.png
---
alt: screenshot of HTML rendering while hovering over the contribution button, contribution options shown are 'repository', 'issue', 'suggest edit'
figclass: bordered
---
Hovering over the contribution button shows all options for contributing to the book.
```

## Repository Button

The repository button is a simple link to the book's GitLab repository. There you find all source files need to render the book. If you are familar with Git and GitLab the repository button is a good starting point for contributing to the project. If you are not familar with Git and GitLab, don't worry and use one of the other contribution options.

```{figure} gitlab.png
---
alt: screenshot of the book's GitLab page appearing after clicking the repository button
figclass: bordered
---
The repository button leads to page in GitLab with information about the project, including information about the most resent update.
```

````{important}
The book's public Git repository is hosted on a GitLab instance provided by [Chemnitz University of Technology](https://www.tu-chemnitz.de) for all Saxon universities. Actions requiring a user account are restricted to members of Saxon universities.
````

## Open Issue Button

The open issue button allows to ask questions or report bugs, typos and so on. Clicking this button opens a new issue in the book's GitLab repository. Put your question, bug report, what ever in the description field and click the 'Create issue' button.

```{figure} open-issue.png
---
alt: screenshot of the book's GitLab page for opening an issue with highlighted description field and highlighted 'Create issue' button
figclass: bordered
---
To open an issue simply type a description and click on 'Create issue'. The title field is prefilled and should remain untouched.
```

The author will have a look at the issue as soon as possible and post an answer. Depending on your GitLab account's configuration you will receive email notifications if someone posts a comment on your issue. Each reader may comment on each other reader's issues. So readers may help other readers to solve problems where appropriate.

## Suggest Edit Button

If you spot a type or if you would like to add some explanatory note or an additional code example you should hit the 'suggest edit' button. The button opens the current page for editing in GitLab. The syntax of the text file is [MyST markdown](https://myst-parser.readthedocs.io) and should be self-explanatory.
If your edit contains more than a typo correction, leave a commit message summarizing your edit. Then click 'Commit changes'.

```{figure} suggest-edit.png
---
alt: screenshot of the book's GitLab page for editing a file with highlighted 'Commit message' field and highlighted 'Commit changes' button
figclass: bordered
---
Edit the pages markdown source and leave a commit message to describe more elaborate edits.
```

On the next page click 'Create merge request'. This will send a notification to the author that somebody suggested an edit.

```{figure} merge-request.png
---
alt: screenshot of the book's GitLab page for creating a merge request with highlighted 'Create merge request' button
figclass: bordered
---
Simply click 'Create merge request'. Fill the description field if you feel a need for additional explanation next to your commit message.
```

The author will have a look on your suggestion as soon as possible. GitLab's merge request feature allows for discussing and modifying the edit if necessary before the author merges the edit into the book's source and, thus, into the published book.

## Other Forms of Contribution

If you don't have an account at the book's GitLab instance feel free to send issues and suggestions for edits to [the author](https://www.fh-zwickau.de/~jef19jdw) by email.
