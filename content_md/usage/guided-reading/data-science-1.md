---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Data Science I (course at WHZ)

The first part of the data science lecture series introduces the Python programming language and some Python libraries required for data processing. Next to Python the focus is on working with big data, obtaining, understanding and restructuring data, as well as extracting basic statistical information from data.

## Warm-Up

### Week 1

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](warm-up:data-science)
* [](warm-up:python)
* [](warm-up:computers-programming)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:computer-basics) (exercises)
* [](projects:install:jupyterlab) (project)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:install:jupyter) (project)
:::
::::

## Python for Data Science

### Week 2 (Crash Course I)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](python:crash)
  * [](python:crash:first-program)
  * [](python:crash:blocks)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:python:finding-errors) (exercises)
* [](exercises:python:basics) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:install:python-without-jupyter) (project)
* [](projects:python:simple-list) (project)
:::
::::

### Week 3 (Crash Course II)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](python:crash), continued
  * [](python:crash:screen-io)
  * [](python:crash:library-code)
  * [](python:crash:everything-object)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:python:more-basics) (exercises, last task is considered bonus)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:python:geometric-objects) (project)
:::
::::

### Week 4 (Variables and Operators)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](python:variables)
  * [](python:variables:names-objects)
  * [](python:variables:types)
  * [](python:variables:operators) (section [](python:variables:operators:operators-member-functions) is bonus)
  * [](python:variables:efficiency) (all but section [](python:variables:efficiency:garbage-collection) is bonus)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:python:variables-operators) (exercises)
* [](exercises:python:memory-management) (exercises, all but the last two tasks are considered bonus)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:weather:dwd-open-data) (project)
* bonus: [](projects:python:vector-multiplication) (project)
:::
::::

### Week 5 (Lists and Friends, Strings)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](python:lists-friends)
  * [](python:lists-friends:tuples)
  * [](python:lists-friends:lists)
  * [](python:lists-friends:dicts)
  * [](python:lists-friends:iterable-objects)

* [](python:strings)
  * [](python:strings:basics)
  * [](python:strings:special-characters)
  * [](python:strings:formatting)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:python:lists-friends) (exercises)
* [](exercises:python:strings) (exercises, last one is bonus)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist:xmnist) (project)
:::
::::

### Week 6 (Accessing Data)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](python:accessing-data)
  * [](python:accessing-data:file-io)
  * [](python:accessing-data:text-files)
  * [](python:accessing-data:zip-files)
  * [](python:accessing-data:csv-files)
  * [](python:accessing-data:html-files)
  * [](python:accessing-data:xml-files)
  * [](python:accessing-data:web-access)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:python:file-access) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:cafeteria), download part (project)
:::
::::

### Week 7 (Functions, Modules, Packages)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](python:functions)
  * [](python:functions:basics)
  * [](python:functions:passing-arguments)
  * [](python:functions:anonymous-functions)
  * [](python:functions:function-method-objects)
  * [](python:functions:recursion)
* [](python:modules-packages)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:python:functions) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:cafeteria), parsing part (project)
:::
::::

### Week 8 (Errors, Debugging, Inheritance)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](python:error-handling-debugging-overview)
* [](python:inheritance) (last section [](python:inheritance:exceptions) is bonus)
* [](software-development:uml) (bonus)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:python:oop) (exercises)
* [](python:further-python-features) (bonus reading)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:weather:getting-forecasts), download part (project)
:::
::::
