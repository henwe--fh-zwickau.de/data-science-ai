---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Data Science II (course at WHZ)

The second semester of the data science lecture series starts with visualization techniques. Then supervised machine learning for generating predictions from data is introduced. Linear regression and artificial neural networks are discussed in depth.

to be continued...
