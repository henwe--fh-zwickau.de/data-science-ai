---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Data Science III (course at WHZ)

Part three of the data science lecture series continues discussion of supervised machine learning. Further methods like decision trees and support vector machines are introduced. Then we move on to unsupervised machine learning covering clustering methods and techniques for dimensionality reduction.

to be continued...
