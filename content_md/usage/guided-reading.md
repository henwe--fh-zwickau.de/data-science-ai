---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Guided Reading

The teaching material in this book, including exercies and projects, may be arranged in different ways to meet the needs of a comprehensive lecture series or to accompany a one-day workshop on machine learning, for instance. In each section the chapter presents a selection of material together with hints on working through it. Studens of Zwickau University's data science course will find the material for each semester here.

```{tableofcontents}
```
