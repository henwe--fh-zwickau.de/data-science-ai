---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:strings)=
# Strings

We already learned basic usage of strings in the [](python:crash). Here we add the details. Correct string handling is essential for loading data from files and also for writing to files.

```{tableofcontents}
```
