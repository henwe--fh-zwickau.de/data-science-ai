---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:further-python-features)=
# Further Python Features

Python provides much more features than we need. Here we list some of them, which might be of interest either because they simplify some coding tasks or because they frequently occur in other people's code.

## Doing Nothing With `pass`

Python does not allow emtpy functions, classes, loops and so on. But there is a 'do nothing' command, the [`pass`](https://docs.python.org/3/reference/simple_stmts.html#the-pass-statement) keyword.

```{code-cell} ipython3
def do_nothing():

    pass
    
    
do_nothing()
```

## Checking Conditions With `assert`

Especially for debugging purposes placing multiple `if` statements can be avoided by using [`assert`](https://docs.python.org/3/reference/simple_stmts.html#the-assert-statement) instead. The condition following `assert` is evaluated. If the result is `False` an `AssertionError` is raised, elso nothing happens. An optional error message is possible, too.

```{code-cell} ipython3
:tags: [raises-exception]

a = 0   # some number from somewhere (e.g., user input)

assert a != 0, 'Zero is not allowed!'
    
b = 1 / 0
```

## Structural Pattern Matching

Python 3.10 introduces two new keywords: `match` and `case`. In the simplest case they can be used to replace `if...elif...elif...else` constructs for discrimating many cases. But in addition they provide a powerful pattern matching mechanism. See [PEP 636 - Structural Pattern Matching: Tutorial](https://peps.python.org/pep-0636/) for an introduction.

## The `set` Type

Python knows a data type for representing (mathematical) sets. Its name is `set`. Typical operations like unions and intersections of sets are supported. The official [Python Tutorial](https://docs.python.org/3/tutorial/datastructures.html#sets) shows basic usage.

## Function Decorators

Function decorators are *syntactic suggar* (that is, not really needed), but very common. They precede a function definition and consist of an `@` character and a function name. Function decorators are used to modify a function by applying another function to it. The following two code cells are more or less equivalent:

```{code-cell} ipython3
def double(func):
    return lambda x: 2 * func(x)

@double
def calc_something(x):
    return x * x
```

```{code-cell} ipython3
def double(func):
    return lambda x: 2 * func(x)

def calc_something(x):
    return x * x
    
calc_something = double(calc_something)
```

See [Function definitions](https://docs.python.org/3/reference/compound_stmts.html#function-definitions) in Python's documention for details.

## The `copy` Module

The [`copy` module](https://docs.python.org/3/library/copy.html) provides functions for shallow and deep copying of objects. For discussion of the copy problem in the context of lists see [](python:lists-friends:lists:copies).
