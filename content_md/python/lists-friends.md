---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:lists-friends)=
# Lists and Friends

We already met lists in the [](python:crash). Now it's time to discuss some details and to introduce further list-like objects types as well as Python features for efficiently working with lists and friends.

```{tableofcontents}
```
