---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:accessing-data)=
# Accessing Data

There exist several sources of data: files, data bases, and web services, for instance. Here we consider basic file access and common file formats for storing large data sets. We also learn how to automatically download data from the web and how to scrape data from websites.

```{tableofcontents}
```
