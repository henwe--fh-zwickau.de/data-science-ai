---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:variables)=
# Variables and Operators

Variables in Python follow a different, much more elegant approach than in other programming languages. In this chapter we discuss the details of variables in Python and of some consequences of Python's approach to variables.

```{tableofcontents}
```
