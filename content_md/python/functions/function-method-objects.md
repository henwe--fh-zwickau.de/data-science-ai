---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:functions:function-method-objects)=
# Function and Method Objects

Everything is an object in Python, even functions and member functions.

```{code-cell} ipython3
def my_function():
    print('Oh, you called me!')
    
print(type(my_function))
print(id(my_function))
print(dir(my_function))
```

```{code-cell} ipython3
print(my_function.__name__)
```

Member functions are bit special.

```{code-cell} ipython3
class my_class:
    def some_method(self, some_string):
        print('You called me with {}'.format(some_string))

my_object = my_class()

print(type(my_object.some_method))
print(type(my_class.some_method))
```

If the method `my_object.some_method` is called, then the Python interpreter inserts the owning object as first argument and calls the corresponding function `my_class.some_method`. In other words, the following two lines are equivalent:

```{code-cell} ipython3
my_object.some_method('Hello')
my_class.some_method(my_object, 'Hello')
```
