---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:functions)=
# Functions

We already met functions in the [](python:crash). Here we repeat the basics and add lots of details important for successful Python programming.

```{tableofcontents}
```
