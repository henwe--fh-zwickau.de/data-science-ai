---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(software-development:uml)=
# Unified Modeling Language (UML)

There's a standard for visualizing the design of software and other systems: unified modeling language (UML). Next to many other types of visualization it standardizes how to express relations between classes graphically. Especially, inheritance relations can be visualized. We do not go into the details here. But you should know that there is a standard and from time to time you should practice reading UML class diagrams, since they are used for planning and communicating larger software projects.

Here is an example:
![example UML class diagram](class-diagram-example.svg)

To get an overview of UML class diagrams have a look at [Class diagram](https://en.wikipedia.org/wiki/Class_diagram) at Wikipedia.

An open source tool for drawing UML class diagrams is [UMLet](https://www.umlet.com).

Other types of diagrams are shown in Wikipedia's article [Unified Modeling Language](https://en.wikipedia.org/wiki/Unified_Modeling_Language).
