{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "817e7e3f",
   "metadata": {},
   "source": [
    "(warm-up:python)=\n",
    "# Python and Jupyter\n",
    "\n",
    "In this book we use the [Python](https://www.python.org) programming language for talking to the computer. Tools from the [Jupyter](https://jupyter.org) allow for Python programming in a very comfortable graphical environment.\n",
    "\n",
    "## Data Science Tools\n",
    "\n",
    "There are lots of software tools for data science and artificial intelligence. They can be devided into two groups:\n",
    "::::{grid}\n",
    "\n",
    ":::{grid-item-card}\n",
    "**Tailor-made GUI tools**\n",
    "^^^\n",
    "For common tasks in data science and AI like clustering data or classifying images there exist (mostly commerical) tools with graphical user interface (GUI). Such tools are easy to use, but they have a very limited scope of application. Each task requires a different tool. Available methods are restricted to well known ones. Implementing new problem specific methods is not possible.\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    "**General Purpose Tools**\n",
    "^^^\n",
    "To enjoy maximum freedom in choice of methods one has to leave the world of GUI tools. Creating data science models (that is, computer programs) without any restrictions requires the use of some high-level programming language. Examples are [R](https://www.r-project.org/) and [Python](https://www.python.org). Both languages are very common in the data science community because they ship with lots of extensions for simple usage in data science and AI.\n",
    ":::\n",
    "::::\n",
    "\n",
    "Tailor-made tools come and go as time moves on. Programming languages are much more long-lasting. In this book we stick to the Python programming language and its eco system. The R programming language would be a good alternativ, but sticks more to statistical tasks than to general purpose programming.\n",
    "\n",
    "````{tip}\n",
    "Some people feel frightend if someone says 'programming language'. Think of programming languages as usual software tools. The only difference is that they provide much more functionality than GUI tools. But there's not enough space on screen to have a button for each function. So we write text commands.\n",
    "````\n",
    "\n",
    "## Why Python?\n",
    "\n",
    "Python is a modern, free and open source programming language. It dates back to the early 1990s with a first official release in 1994. It's father and BDFL (benevolent dictator for life) is [Guido van Rossum](https://en.wikipedia.org/wiki/Guido_van_Rossum).\n",
    "\n",
    "```{figure} xkcd353.png\n",
    "---\n",
    "alt: line drawing showing a person on ground wondering why the other is flying\n",
    "---\n",
    "I wrote 20 short programs in Python yesterday. It was wonderful. Perl, I'm leaving you. Source: Randall Munroe, [xkcd.com/353](https://xkcd.com/353)\n",
    "```\n",
    "\n",
    "Python code is very readable and straight forward without too many cumbersome symbols like in most other programming languages. Many technical aspects of computer programming are managed by Python instead of by the programmer. With Python one may develop the full range of software, from simple scripts to fully featured web or desptop applications. Thousands of extensions allow for rapid development.\n",
    "\n",
    "There's a large online community discussing Python topics. Almost every problem you'll encounter has already been solved. Simply use a search engine to find the answer.\n",
    "\n",
    "```{figure} languages.svg\n",
    "---\n",
    "alt: line chart showing the popularity of programming languages from 2008 till 2022\n",
    "scale: 120%\n",
    "---\n",
    "Popuparity of programming languages on [Stack Overflow](https://stackoverflow.com). Source: [Stack Overflow Trends](https://insights.stackoverflow.com/trends?tags=python%2Cjavascript%2Cjava%2Cc%23%2Cphp%2Cc%2B%2B%2Cr) (modified by the author)\n",
    "```\n",
    "\n",
    "Some rules followed by Python and its community are collected in the [Zen of Python](https://en.wikipedia.org/wiki/Zen_of_Python). Here are some of them:\n",
    "* Beautiful is better than ugly.\n",
    "* Explicit is better than implicit.\n",
    "* Simple is better than complex.\n",
    "* Complex is better than complicated.\n",
    "* Readability counts.\n",
    "* There should be one – and preferably only one – obvious way to do it.\n",
    "* Although that way may not be obvious at first unless you're Dutch.\n",
    "* If the implementation is hard to explain, it's a bad idea.\n",
    "* If the implementation is easy to explain, it may be a good idea.\n",
    "\n",
    "Last but not least Python is available on all platforms, Linux, macOS, Windows, and many more. Youtube's player is written in Python and many other tech giants use Python. But it's also not unlikely that a Python script controls your washing machine.\n",
    "\n",
    "````{hint}\n",
    "There are two versions of Python: Python 2 and Python 3. Source code is not compatible, that is, there are programs written in Python 2 which cannot be executed by a Python 3 interpreter. In this book we stick to Python 3. [Python 2 is considered deprecated since January 2020](https://www.python.org/doc/sunset-python-2/).\n",
    "````\n",
    "\n",
    "## Jupyter\n",
    "\n",
    "The Jupyter ecosystem is a collection of tools for Python programming with emphasis on data science. Jupyter allows for Python programming in a web browser. Outputs, including complex and interactive visualizations, can be but right below the code producing these outputs. Everything is in one documents: code, outputs, text, images,...\n",
    "\n",
    "```{figure} jupyterlab.png\n",
    "---\n",
    "alt: screenshot of JupyterLab with running notebook\n",
    "figclass: bordered\n",
    "---\n",
    "JupyterLab is the most widely used member of the Jupyter ecosystem. It brings Python to the webbrowser.\n",
    "```\n",
    "\n",
    "In this book you'll meet at least four members of the Jupyter ecosystem.\n",
    "\n",
    "::::{grid} 2\n",
    ":gutter: 4\n",
    "\n",
    ":::{grid-item-card}\n",
    "**[JupyterLab](https://jupyter.org)**\n",
    "^^^\n",
    "JupyterLab is a web application bringing Python programming to the browser. It's the everyday tool for data science. JupyterLab may run on a remote server (cloud) or on your local machine.\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    "**[Jupyter Notebook](https://jupyter.org)**\n",
    "^^^\n",
    "An alternative to JupyterLab is Jupyter Notebook. It's a predecessor of JupyterLab and provides almost identical functionality, but with different look and feel.\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    "**[JupyterHub](https://jupyter.org/hub)**\n",
    "^^^\n",
    "Running JupyterLab in the cloud requires user authentication and user management. JupyterHub provides everything we need to run several JupyterLabs on a server in parallel. Almost all JupyterLab providers (e.g., Gauss at Zwickau University, Binder) rely on JupyterHub.\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    "**[Jupyter Book](https://jupyterbook.org)**\n",
    "^^^\n",
    "This book is being published using Jupyter Book. Each page is a Jupyter notebook file. Jupyter Book provides automatic generation of table of contents, handling bibliographies and rendering to different output formats.\n",
    ":::\n",
    "::::\n",
    "\n",
    "## Install and Use\n",
    "\n",
    "Work through the following projects to get up and running with Python and Jupyter:\n",
    "* [](projects:install:jupyterlab)\n",
    "* [](projects:install:jupyter)\n",
    "* [](projects:install:python-without-jupyter)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
