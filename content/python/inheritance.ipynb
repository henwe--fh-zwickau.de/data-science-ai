{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ac64d86c",
   "metadata": {},
   "source": [
    "(python:inheritance)=\n",
    "# Inheritance\n",
    "\n",
    "Inheritance is an important principle in object-oriented programming. Although we may reach our aims without using inheritance in our own code, it's important to know the concept and corresponding syntax constructs to understand other people's code. We'll meet inheritance related code in\n",
    "* the documentation of modules and packages,\n",
    "* when customizing and extending library code.\n",
    "\n",
    "## Principles of Object-Oriented Programming\n",
    "\n",
    "Up to now we only considered three of four fundamental OOP principles:\n",
    "* *encapsulation*: code is structured into small units (write functions for different tasks and group them together with relevant data into objects)\n",
    "* *abstraction*:\n",
    "  * similar tasks are handled in one and the same way (instead of several unrelated objects we define a class and instantiate several objects sharing the same interface)\n",
    "  * implementation details are hidden behind interfaces (a class defines an interface by providing methods and (non-private) member variables; concrete implementation of methods and usage of member variables are not of importance and invisible from outside)\n",
    "* *polymorphism* (functions and method accept different sets and types of arguments, that is, interfaces are flexible; something a Python programmer does not care about because it's a very native Python feature, in contrast to C/C++, for instance)\n",
    "  \n",
    "The missing principle is\n",
    "* *inheritance* (create new classes by extending existing classes)\n",
    "\n",
    "## Idea and Syntax\n",
    "\n",
    "Inheritance is a technique to create new classes by extending and/or modifying existing ones. A new class may have a *base class*. The new class inherits all methods and member variables from its base class and is allowed to replace some of the methods and to add new ones. Syntax:\n",
    "```python\n",
    "class NewClass(BaseClass):\n",
    "    \n",
    "    def additional_method(self, args):\n",
    "        # do something\n",
    "    \n",
    "    def replacement_for_base_class_method(self, args):\n",
    "        # do something\n",
    "```\n",
    "The only difference compared to usual class definitions is in the first line, where a base class can be specified. Defining methods works as before. If the method name does not exist in the base class, then a new method is created. If it already exists in the base class, the new one is used instead of the base class' method. In addition to explicitly defined methods, the new class inherits all methods from the base class.\n",
    "\n",
    "Inheritance saves time for implementation and leads to a well structured class hierachy. Object-oriented programming is not solely about defining classes (encapsulation and abstraction), but also about defining meaningful relations between classes, thus, to some extent mapping real world to source code.\n",
    "\n",
    "## Example\n",
    "\n",
    "Real-life examples of inheritance often are quite involved. For illustration we use a pathological example resampling relations between geometric objects.\n",
    "\n",
    "Imagine a vector drawing program. Each geometric object shall be represented as object of a corresponding class. Say quadrangles are objects of type `Quad`, paraxial rectangles are objects of type `ParRect` and so on. Let's start with class `Point`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "accbea23",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Point:\n",
    "    ''' represent a geometric point in two dimensions '''\n",
    "    \n",
    "    def __init__(self, x, y):\n",
    "        self.x = x\n",
    "        self.y = y\n",
    "    \n",
    "    def __str__(self):\n",
    "        return f'({self.x}, {self.y})'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0619cba",
   "metadata": {},
   "source": [
    "Now we define `Quad`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e5c9bdc3",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Quad:\n",
    "    ''' represent a quadrangle '''\n",
    "    \n",
    "    def __init__(self, a, b, c, d):\n",
    "        ''' make a quad from 4 Point objects '''\n",
    "        self._a = a\n",
    "        self._b = b\n",
    "        self._c = c\n",
    "        self._d = d\n",
    "    \n",
    "    def get_points(self):\n",
    "        return (self._a, self._b, self._c, self._d)\n",
    "    \n",
    "    def __str__(self):\n",
    "        return f'quadrangle with points' \\\n",
    "               f'({self._a.x}, {self._a.y}), ({self._b.x}, {self._b.x}), ' \\\n",
    "               f'({self._c.x}, {self._c.x}), ({self._d.x}, {self._d.x})'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c6dd792",
   "metadata": {},
   "source": [
    "The member variables `_a`, `_b`, `_c`, `_d` are hidden since we consider them implementation details. If the user wants access to the four points making the quadrangle, `get_points` should be called. This way we are free to store the quadrangle in a different format if it seems resonable in future when extending class' functionality. This is a design decision and is in no way related to inheritance.\n",
    "\n",
    "Here comes `ParRect`: Note that a paraxial rectangle is defined by two Points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64277126",
   "metadata": {},
   "outputs": [],
   "source": [
    "class ParRect(Quad):\n",
    "    \n",
    "    def __init__(self, a, c):\n",
    "        ''' make a paraxial rect from two points '''\n",
    "        b = Point(c.x, a.y)\n",
    "        d = Point(a.x, c.y)\n",
    "        super().__init__(a, b, c, d)\n",
    "        \n",
    "    def __str__(self):\n",
    "        return f'paraxial rect with points ({self._a.x}, {self._a.y}), ({self._c.x}, {self._c.x})'\n",
    "    \n",
    "    def area(self):\n",
    "        ''' return the rect's area '''\n",
    "        return abs(self._b.x - self._a.x) * abs(self._d.y - self._a.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd8d4813",
   "metadata": {},
   "source": [
    "The `ParRect` class inherits everything from `Quad`. It has a new constructor with fewer arguments than in `Quad`, but calls the constructor of `Quad`.\n",
    "\n",
    "```{important}\n",
    "The built-in function `super` in principle returns `self` (that is, the current object), but redirects method calls to the base class.\n",
    "```\n",
    "\n",
    "We reimplement `__str__` and add the new method `area`. Note that `ParRect` objects have member variables `_a`, `_b`, `_c`, `_d` since those are created by the `Quad` constructor we call in the `ParRect` constructor. Also the `get_points` method is a  member of `ParRect` since it gets inherited from `Quad`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08f00361",
   "metadata": {},
   "outputs": [],
   "source": [
    "parrect = ParRect(Point(0, 0), Point(2, 1))\n",
    "\n",
    "print(parrect)\n",
    "\n",
    "print('area: {}'.format(parrect.area()))\n",
    "\n",
    "a, b, c, d = parrect.get_points()\n",
    "print('all points:', a, b, c, d)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b193c57",
   "metadata": {},
   "source": [
    "## Type Checking\n",
    "\n",
    "Note that `isinstance` also returns `True` if we check against a base class of an object's class. In other words, each object is an instance of its class and of all base classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "177e5fd6",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(isinstance(parrect, ParRect))\n",
    "print(isinstance(parrect, Quad))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b105a298",
   "metadata": {},
   "source": [
    "In contrast, type checking with `type` returns `False` if checked against the base class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "98dd42e9",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(parrect) == ParRect)\n",
    "print(type(parrect) == Quad)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10d2226f",
   "metadata": {},
   "source": [
    "## Every Class is a Subclass of `object`\n",
    "\n",
    "In Python there is a built-in class `object` and every newly created class automatically becomes a subclass of `object`. The line\n",
    "```python\n",
    "class my_new_class:\n",
    "```\n",
    "is equivalent to\n",
    "```python\n",
    "class my_new_class(object):\n",
    "```\n",
    "and also to\n",
    "```python\n",
    "class my_new_class():\n",
    "```\n",
    "by the way.\n",
    "\n",
    "To see this in code we might use the built-in function `issubclass`. This function returns `True` if the first argument is a subclass of the second."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9dc78512",
   "metadata": {},
   "outputs": [],
   "source": [
    "class MyClass:\n",
    "    \n",
    "    def __init__(self):\n",
    "        print('Here is __init__()!')\n",
    "\n",
    "print(issubclass(MyClass, object))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "289ced66",
   "metadata": {},
   "source": [
    "Alternatively, we may have a look at the `__base__` member variable, which stores the base class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41934639",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(MyClass.__base__)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "265b9961",
   "metadata": {},
   "source": [
    "Objects of type `object` do not have real functionality. The `object` class provides some auxiliary stuff used by the Python interpreter managing classes and objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6df46487",
   "metadata": {},
   "outputs": [],
   "source": [
    "obj = object()\n",
    "dir(obj)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a46da0a8",
   "metadata": {},
   "source": [
    "## Virtual Methods\n",
    "\n",
    "Python does not allow for directly implementing so called *virtual methods*. A virtual method is a method in a base class which has to be (re-)implemented by each subclass. The typical situation is as follows: The base class implements some functionality, which for some reason has to call a method of a subclass. How to guarantee that the subclass provides the required method?\n",
    "\n",
    "In Python a virtual method is a usual method which raises a `NotImplementedError`, a special exception type like `ZeroDivisionError` and so on. If everything is correct, this never happens, because the subclass overrides the base class' method. But if the creator of the subclass forgets to implement the method required by the base class, an error message will be shown.\n",
    "\n",
    "## Multiple Inheritance\n",
    "\n",
    "A class may have several base classes. Just provide a tuple of base classes in the class definition:\n",
    "```python\n",
    "class my_class(base1, base2, base3):\n",
    "```\n",
    "The new class inherits everything from all its base classes.\n",
    "\n",
    "If two base classes provide methods with identical names, the Python interpreter has to decide which one to use for the new class. There is a well-defined algorithm for this decision. If you need this knowledge someday, watch out for *method resolution order (MRO)*.\n",
    "\n",
    "(python:inheritance:exceptions)=\n",
    "## Exceptions Inherit from `Exception`\n",
    "\n",
    "Up to now we used built-in exceptions only, like `ZeroDivisionError`. But now we have gathered enough knowledge to define new exceptions. Exeptions are classes as we noted before. Each exception is a direct or indirect subclass of `BaseException`. Almost all exceptions also are a subclass of `Exception`, which itself is a direct subclass of `BaseException`. See [Exception hierarchy](https://docs.python.org/3/library/exceptions.html#exception-hierarchy) for exceptions' genealogy.\n",
    "\n",
    "If we want to introduce a new exception, we have to create a new subclass of `Exception`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "346bf7c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "class SomeError(Exception):\n",
    "    \n",
    "    def __init__(self, message):\n",
    "        self.message = message\n",
    "    \n",
    "def my_function():\n",
    "    print('I do something...')\n",
    "    raise SomeError('Meaty error message!!!')\n",
    "    \n",
    "print('Entering my_function...')\n",
    "try:\n",
    "    my_function()\n",
    "except SomeError as error:\n",
    "    print('Exception SomeError: {}'.format(error.message))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91dbc534",
   "metadata": {},
   "source": [
    "At first we define a new exception class `SomeError`. The constructor takes an error message and stores it in the member variable `message`.\n",
    "The function `my_function` raises `SomeError`.\n",
    "The main program catches this exception and prints the error message. The `as` keyword provides access to a concrete `SomeError` object containing the error message.\n",
    "\n",
    "Note that `except SomeBaseClass` also catches all subclasses of `SomeBaseClass`. If we want to handle a subclass exception separately we have to place its `except` line above the base class's `except` line.\n",
    "Contrary, a subclass `except` never handles a base class exception."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
