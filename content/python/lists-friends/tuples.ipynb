{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d810f018",
   "metadata": {},
   "source": [
    "(python:lists-friends:tuples)=\n",
    "# Tuples\n",
    "\n",
    "Tuples are *immutable* objects which hold a *fixed-size* list of other objects. Imagine tuples as a list of pointers to objects, where the number of pointers and the pointers themselve cannot be changed. But note, that the objects the tuple entries point to can change if they are mutable. Object type for tuples is `tuple`.\n",
    "\n",
    "## Syntax\n",
    "\n",
    "Tuples are defined as a comma separated list. Often the list is surrounded by parentheses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9797605a",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = (1, 5, 9)                            # a tuple of integers\n",
    "b = 2, 3, 4                              # works also without parentheses\n",
    "c = ('some string', 'another_string')    # tuple of strings\n",
    "d = (42, 'some string', 1.23)            # tuple with mixed types"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82e2b2e6",
   "metadata": {},
   "source": [
    "Tuples with only one item are allowed, too. To distinguish them from single objects, a trailing comma is required."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "84689ce2",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1       # integer     \n",
    "b = (1)     # integer\n",
    "c = 1,      # tuple containing one integer\n",
    "d = (1,)    # tuple containing one integer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f95204f",
   "metadata": {},
   "source": [
    "## Indexing\n",
    "\n",
    "Tuple items can be accessed by index. The first item has index 0, the second index 1, and so on. The length of a tuple is returned by the built-in function `len`. Indexing with negative numbers gives the items in reverse order."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "5d0308fc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "blue\n",
      "yellow\n",
      "4\n",
      "('red', 'green', 'blue', 'yellow')\n"
     ]
    }
   ],
   "source": [
    "colors = ('red', 'green', 'blue', 'yellow')\n",
    "\n",
    "print(colors[2])\n",
    "print(colors[-1])\n",
    "print(len(colors))\n",
    "print(colors)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb68159a",
   "metadata": {},
   "source": [
    "Subtuples can be extracted by so called *slicing*. Simply provide a range of indices: `[2:5]` gives a new tuple consisting of the items 2, 3, 4. The new tuple is a new object and remains available even if the original tuple vanishes (cf. [](python:variables:efficiency:garbage-collection))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "31f4e2fd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "green\n",
      "('green', 'blue')\n"
     ]
    }
   ],
   "source": [
    "some_colors = colors[1:3]\n",
    "print(some_colors[0])\n",
    "print(some_colors)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e8e5afe",
   "metadata": {},
   "source": [
    "Extracting every second item can be done by `[3:10:2]`, which gives items with indices 3, 5, 7, 9. The general syntax is `[first_index:last_index_plus_one:step]`. Here are some more indexing examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "29c830a0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "('blue', 'yellow')\n",
      "('blue', 'yellow')\n",
      "('blue',)\n",
      "('red', 'green', 'blue')\n",
      "('red', 'green', 'blue', 'yellow')\n"
     ]
    }
   ],
   "source": [
    "print(colors[2:])               # from 2 to end\n",
    "print(colors[2:len(colors)])    # from 2 to end\n",
    "print(colors[2:-1])             # from 2 to last but one\n",
    "print(colors[:3])               # from 0 to 3\n",
    "print(colors[:])                # all"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76b58854",
   "metadata": {},
   "source": [
    "## Tuple Assignments\n",
    "\n",
    "Tuples can be used for returning more than one value from a function. For this purpose Python provides the following code construct:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "378d3a91",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "23 42 6\n"
     ]
    }
   ],
   "source": [
    "a, b, c = 23, 42, 6\n",
    "print(a, b, c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0586ed1",
   "metadata": {},
   "source": [
    "That is, we can assigne the contents of a tuple to a tuple of names. Such constructions typically are used if a function returns several return values packed into a tuple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "4622c927",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "7\n",
      "9\n"
     ]
    }
   ],
   "source": [
    "def integer_division(a, b):\n",
    "\n",
    "    c = a // b\n",
    "    d = a % b\n",
    "\n",
    "    return c, d\n",
    "    \n",
    "quotient, remainder = integer_division(100, 13)\n",
    "\n",
    "print(quotient)\n",
    "print(remainder)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f91b5804",
   "metadata": {},
   "source": [
    "## Tuples From Lists\n",
    "\n",
    "Lists may be converted to tuples via usual type convertion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "c54d6d04",
   "metadata": {},
   "outputs": [],
   "source": [
    "some_list = [1, 2, 3, 4, 5]\n",
    "some_tuple = tuple(some_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c6644d1",
   "metadata": {},
   "source": [
    "Note that both list and tuple point to the some items. Items aren't copied! Modifying (mutable) list items will modify tuple items, too. See [](python:lists-friends:lists:copies) for more details."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
