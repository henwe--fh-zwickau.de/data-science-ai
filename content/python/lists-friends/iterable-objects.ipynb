{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "98f510b3",
   "metadata": {},
   "source": [
    "(python:lists-friends:iterable-objects)=\n",
    "# Iterable Objects\n",
    "\n",
    "Tuples, lists, and dictionaries are examples of iterable objects. These are objects which allow for consecutive evalution of their items. There exist more types of iterable objects in Python and we may define new iterable objects by implementing suitable dunder methods.\n",
    "\n",
    "## For loops\n",
    "\n",
    "We briefly mentioned for loops in the [](python:crash). Now we add the details.\n",
    "\n",
    "### Basic Iteration\n",
    "\n",
    "For loops allow to iterate through iterable objects of any kind, especially tuples, lists and dictionaries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "db1fd806",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "red\n",
      "green\n",
      "blue\n",
      "yellow\n"
     ]
    }
   ],
   "source": [
    "colors = ('red', 'green', 'blue', 'yellow')\n",
    "\n",
    "for color in colors:\n",
    "    print(color)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "173f5e36",
   "metadata": {},
   "source": [
    "The indented code below the `for` keyword is executed as long as there are remaining items in the iterable object. In the example above the name `color` first points to `colors[0]`, then, in the second run, to `colors[1]`, and so on.\n",
    "\n",
    "This index-free iteration is considered *pythonic*: it's much more readable than indexing syntax and directly fulfills the purpose of iteration, while indexing in most cases is useless additional effort.\n",
    "\n",
    "```{note}\n",
    "The `range` built-in function we saw in the crash course is not part of the for loop's syntax. Instead, it's a built-in function returning an iterable object which contains a series of integers as specified in the arguments passed to the `range` function. The returned object is of type `range`.\n",
    "\n",
    "The `range` function takes up to three arguments: the starting index, the stopping index plus 1, and the step size. Step size defaults to 1 if omitted. If only one argument is provided, it's interpreted as stopping index (plus 1) and start index is set to 0.\n",
    "```\n",
    "\n",
    "### Iteration With Indices\n",
    "\n",
    "If next to the items themselve also their index is needed inside the loop, use the built-in function `enumerate` and pass the iterable object to it. The `enumerate` function will return an iterable object which yields a 2-tuple in each iteration. The first item of each tuple is the index, the second one is the corresponding object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "00def771",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0: red\n",
      "1: green\n",
      "2: blue\n",
      "3: yellow\n"
     ]
    }
   ],
   "source": [
    "colors = ('red', 'green', 'blue', 'yellow')\n",
    "\n",
    "for index, color in enumerate(colors):\n",
    "    print(str(index) + ': ' + color)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ef191c0",
   "metadata": {},
   "source": [
    "### Iterating Over Multiple Lists in Parallel\n",
    "\n",
    "Being new to Python one is tempted to indexing for iteration over multiple lists in parallel:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "aec5495f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "John Doe\n",
      "Max Muller\n",
      "Lisa Lang\n"
     ]
    }
   ],
   "source": [
    "# non-pythonic!\n",
    "\n",
    "names = ['John', 'Max', 'Lisa']\n",
    "surnames = ['Doe', 'Muller', 'Lang']\n",
    "\n",
    "for i in range(len(names)):\n",
    "    print(names[i] + ' ' + surnames[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7cf13a6f",
   "metadata": {},
   "source": [
    "For iterating over multiple lists at the same time pass them to `zip`. This built-in function returns an iterable objects which yields tuples. The first tuple consists of the first elements of all lists, the second of the second elements of all lists and so on. The returned iterable object has as many items as the shortest list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "95c87909",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "John Doe\n",
      "Max Muller\n",
      "Lisa Lang\n"
     ]
    }
   ],
   "source": [
    "names = ['John', 'Max', 'Lisa']\n",
    "surnames = ['Doe', 'Muller', 'Lang']\n",
    "\n",
    "for name, surname in zip(names, surnames):\n",
    "    print(name + ' ' + surname)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4be105b6",
   "metadata": {},
   "source": [
    "## Comprehensions\n",
    "\n",
    "Applying some operation to each item of an iterable object can be done via for loops. But there are handy short-hands known as *list comprehensions* and *dictionary comprehensions*.\n",
    "\n",
    "### List Comprehensions\n",
    "\n",
    "General syntax is\n",
    "\n",
    "```python\n",
    "[new_item for item in list]\n",
    "```\n",
    "\n",
    "The following code snipped generates a list of squares from a list of numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "303645ea",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[4, 16, 36, 64]\n"
     ]
    }
   ],
   "source": [
    "some_numbers = [2, 4, 6, 8]\n",
    "\n",
    "squares = [x * x for x in some_numbers]\n",
    "\n",
    "print(squares)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb1da6ca",
   "metadata": {},
   "source": [
    "Like in for loops also multiple lists are possible:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "6858c56c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2, 8, 18, 32]\n"
     ]
    }
   ],
   "source": [
    "some_numbers = [2, 4, 6, 8]\n",
    "more_numbers = [1, 2, 3, 4]\n",
    "\n",
    "products = [x * y for x, y in zip(some_numbers, more_numbers)]\n",
    "\n",
    "print(products)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "527d1cc6",
   "metadata": {},
   "source": [
    "Alternative syntax for multiple lists is\n",
    "\n",
    "```python\n",
    "[new_item for item_a in list_a for item_b in list_b]\n",
    "```\n",
    "\n",
    "Same principle works for more than two lists, too.\n",
    "\n",
    "```{note}\n",
    "List comprehensions have two advantages compared to for loops:\n",
    "* For small loops a one-liner is more readable than several lines.\n",
    "* In most cases list comprehensions are faster.\n",
    "```\n",
    "\n",
    "### Dictionary Comprehensions\n",
    "\n",
    "Dict comprehensions look very similar to list comprehensions. General syntax:\n",
    "\n",
    "```python\n",
    "[new_key: new_value for some_item in some_iterable]\n",
    "```\n",
    "\n",
    "Example modifying values only:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "479d457d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'name': '*John*', 'surname': '*Doe*', 'eyes': '*brown*'}\n"
     ]
    }
   ],
   "source": [
    "person = {'name': 'John', 'surname': 'Doe', 'eyes': 'brown'}\n",
    "\n",
    "# enclose all values with star symbols\n",
    "stars = {key: '*' + value + '*' for key, value in person.items()}\n",
    "\n",
    "print(stars)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3fac5bdb",
   "metadata": {},
   "source": [
    "Example modifying keys and values of a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "dc311b35",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'*name*': '*John*', '*surname*': '*Doe*', '*eyes*': '*brown*'}\n"
     ]
    }
   ],
   "source": [
    "person = {'name': 'John', 'surname': 'Doe', 'eyes': 'brown'}\n",
    "\n",
    "# enclose keys and values with star symbols\n",
    "stars = {'*' + key + '*': '*' + value + '*' for key, value in person.items()}\n",
    "\n",
    "print(stars)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bdb17fc2",
   "metadata": {},
   "source": [
    "```{note}\n",
    "Dictionary comprehensions may be used to create new dictionaries from arbitrary iterable objects. For instance we could loop over the `zip`ped lists, one holding the keys and one holding the values.\n",
    "```\n",
    "\n",
    "### Conditional Comprehensions\n",
    "\n",
    "List and dictionary comprehensions can be extended by a condition: simply append `if some_condition` to the comprehension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "3a8505c1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2.0, 3.0, 2.6666666666666665]\n"
     ]
    }
   ],
   "source": [
    "some_numbers = [2, 4, 6, 8]\n",
    "more_numbers = [1, 0, 2, 3]\n",
    "\n",
    "quotients = [x / y for x, y in zip(some_numbers, more_numbers) if y != 0]\n",
    "\n",
    "print(quotients)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26e79474",
   "metadata": {},
   "source": [
    "The list (or dictionary) comprehension drops all items not satisfying the condition.\n",
    "\n",
    "## Manual iteration\n",
    "\n",
    "Python has a built-in function `next` which allows to iterate through iterable objects step by step. For this purpose we first have to create an iterator object from our iterable object. This is done by the built-in function `iter`. Then the iterator object is passed to `next`. The iterator object takes care about what the next item is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "3691e32a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n",
      "2\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "a = iter([3, 2, 5])\n",
    "\n",
    "print(next(a))\n",
    "print(next(a))\n",
    "print(next(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a696eeb6",
   "metadata": {},
   "source": [
    "Creation of the intermediate iterator object is done automatically if iterable objects are used in for loops and comprehensions.\n",
    "\n",
    "## The `in` keyword\n",
    "\n",
    "To test whether an object is contained in an iterable object, we my use the `in` keyword."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "fd0f00e8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "a = [1, 5, 6, 8]\n",
    "print(1 in a)\n",
    "print(10 in a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "b5e0d506",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "False\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "a = {'name': 'Jon', 'age': 42}\n",
    "print('name' in a)\n",
    "print('Jon' in a)\n",
    "print('Jon' in a.values())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "451c1d14",
   "metadata": {},
   "source": [
    "The counterpart to `in` is `not in`.\n",
    "\n",
    "```{note}\n",
    "Conditions `a not in b` and `not a in b` are equivalent. The first uses the `not in` operator, while the second uses `in` and then applies the logical operator `not` to the result.\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
