{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d0a1e4c3",
   "metadata": {},
   "source": [
    "(python:variables:operators)=\n",
    "# Operators\n",
    "\n",
    "Like most other programming languages Python offers lots of operators to form new data from existing one. Important classes of operators are\n",
    "* arithmetic operators (`+`, `-`, `*`, `/`,...),\n",
    "* comparison operators (`==`, `!=`, `<`, `>`,...),\n",
    "* logical operators (`and`, `or`, `not`,...).\n",
    "\n",
    "## Operator Precedence\n",
    "\n",
    "Expressions containing more than one operator are evaluated in well-defined order. Python's operators can be listed from highest to lowest priority. Operators with identical priority are evaluated from left to right.\n",
    "| Syntax | Operator |\n",
    "|------|------|\n",
    "| `**` | exponentiation |\n",
    "| `+`, `-` (unary) | sign |\n",
    "| `*`, `/`, `//`, `%` | multiplication, division |\n",
    "| `+`, `-` (binary) | addition, substraction |\n",
    "| `==`, `!=`, `<`, `>`, `<=`, `>=` | comparison |\n",
    "| `not` | logical not |\n",
    "| `and` | logical and |\n",
    "| `or` | logical or |\n",
    "\n",
    "See [Python's documentation](https://docs.python.org/3/reference/expressions.html#operator-precedence) for a complete list of all operators.\n",
    "\n",
    "## Chained Comparisons\n",
    "\n",
    "We may write chained comparions like `a < b < c`. Python interprets them as single comparisons connected by `and`, that is, `a < b and b < c`.\n",
    "\n",
    "```{note}\n",
    "Unfortunate expressions like `a < b > c` are allowed, too. This example is equivalent to `a < b and b > c`. In a chain only neighboring operands are compared to each other! There is no comparison between `a` and `c` here.\n",
    "```\n",
    "\n",
    "## Augmented Assignments\n",
    "\n",
    "For arithmetic binary operators there's a shortcut for expressions like `a = a + b`. We may write such as `a += b`. The latter is called an *augmented assignment*. Although the result will look the same, there are two technical differences one should know:\n",
    "* augmented assignment may work in-place,\n",
    "* for augmented assignment the assignment target will be evaluated only once.\n",
    "\n",
    "### In-place Computations\n",
    "\n",
    "For usual binary operators the Python interpreter calls corresponding dunder methods, like `__add__` for `+`. Augmented assignments have their own dunder methods starting with `i`. So `+=` calls `__iadd__`, for instance. The intention is that `+=` may work in-place, that is, without creating a new object. Of course, this is only possible for mutable objects. If there is no dunder method for augmented assignment the interpreter falls back to the usual binary operator's dunder method.\n",
    "\n",
    "**Example:** The `+` operator applied to two lists concatenates both lists. With `a = a + b` a new list object holding the result is created and then the name `a` is tied to the new object. With `a += b` list `b` is appended to the existing list object referred to by `a`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9af81a3c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140044513799104\n",
      "[1, 2, 3, 4, 5, 6]\n",
      "140044513800000\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3]\n",
    "b = [4, 5, 6]\n",
    "\n",
    "print(id(a))\n",
    "\n",
    "a = a + b\n",
    "\n",
    "print(a)\n",
    "print(id(a))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "382b5f21",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140044513799104\n",
      "[1, 2, 3, 4, 5, 6]\n",
      "140044513799104\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3]\n",
    "b = [4, 5, 6]\n",
    "\n",
    "print(id(a))\n",
    "\n",
    "a += b\n",
    "\n",
    "print(a)\n",
    "print(id(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f16ef53",
   "metadata": {},
   "source": [
    "### Only One Evaluation\n",
    "\n",
    "If the assignment target is a more complex expression like for list items, the expression will be evaluated twice with usual binary operators, but only once if augmented assignment is used.\n",
    "\n",
    "**Example:** In the following code an item of a list shall be incremented by 1. The item's index is computed by some complex function `get_index` (which for demonstration purposes is very simple here). The two code cells show different implementations, resulting in a different number of calls to `get_index`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "8defcced",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "get_index called\n",
      "get_index called\n",
      "[1, 2, 4, 4]\n"
     ]
    }
   ],
   "source": [
    "def get_index():\n",
    "    print('get_index called')\n",
    "    return 2\n",
    "    \n",
    "a = [1, 2, 3, 4]\n",
    "\n",
    "a[get_index()] = a[get_index()] + 1\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "7a840413",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "get_index called\n",
      "[1, 2, 4, 4]\n"
     ]
    }
   ],
   "source": [
    "def get_index():\n",
    "    print('get_index called')\n",
    "    return 2\n",
    "    \n",
    "a = [1, 2, 3, 4]\n",
    "\n",
    "a[get_index()] += 1\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0442239b",
   "metadata": {},
   "source": [
    "```{note}\n",
    "If efficiency matters you should prefer augmented assignments. Even `a += b` with integers `a` and `b` is more efficient than `a = a + b`, because the name `a` will be looked up only once in the table mapping names to object IDs.\n",
    "```\n",
    "\n",
    "(python:variables:operators:operators-member-functions)=\n",
    "## Operators as Member Functions\n",
    "\n",
    "All Python operators, `==` and `+` for instance, simply call a specially named member function of the involved objects, a so called dunder method. Lines 3 and 4 of the following code cell do exactly the same thing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "e8abeda4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "7\n",
      "7\n"
     ]
    }
   ],
   "source": [
    "a = 5\n",
    "\n",
    "b = a + 2\n",
    "c = a.__add__(2)\n",
    "\n",
    "print(b)\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8adc7db",
   "metadata": {},
   "source": [
    "Dunder methods allow to create new object types which can implement all the Python operators themselve. What an operator does depends on the operands' object type. For instance, `+` applied to numbers is usual addition, but `+` applied to strings is concatenation.\n",
    "\n",
    "### Dunder Methods for Binary Operators\n",
    "\n",
    "For binary operators like `+` and `==` there is always the question which of both object to use for calling the corresponding dunder method. In case of comparisons Python uses the dunder method of the left-hand side operand (up to one minor exception we don't discuss here). For arithmetic operations Python always tries the left operand first (again, we omit a minor exception). If it does not have the required dunder method, then Python tries the operand on the right-hand side. If both objects do not have the dunder method, then then interpreter stops with an error.\n",
    "\n",
    "Binary arithmetic operations might be unsymmetric. Thus, there are two variants of most arithmetic dunder methods: one for applying an operation as the left-hand side operand and one for applying an operation as the right-hand side operand. For addition the methods are called `__add__` and `__radd__`, for multiplication we have `__mul__` and `__rmul__`. Others follow the same scheme.\n",
    "\n",
    "### Operands of Different Types\n",
    "\n",
    "Often binary operators shall be applied to objects of different types (adding integer and floating point values, for instance). Even if both objects have the corresponding dunder method, one or both of them could lack code for handling certain object types.\n",
    "\n",
    "In such a case Python calls the dunder method and the method returns `NotImplemented` to signal that it don't know how to handle the other operand. Then the interpreter tries the dunder method of the other operand. If it returns `NotImplemented`, too, then the interpreter stops with an error. The `__add__` function of integer objects cannot handle `float` objects, but `__add__` of `float` objects can handle integers, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "fa575de4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NotImplemented\n",
      "3.23\n"
     ]
    }
   ],
   "source": [
    "a = 2\n",
    "b = 1.23\n",
    "print(a.__add__(b))\n",
    "print(b.__add__(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a63e9f0a",
   "metadata": {},
   "source": [
    "Writing `a + b` in the example above first calls `a.__add__(b)`, which returns `NotImplemented`, then `b.__radd__`. With `b + a` the first call goes to `b.__add__` and no second call (to `a.__radd__`) is required.\n",
    "\n",
    "An example of beneficial use of Python's flexible mechanism for customizing operators is discussed in the project on [](projects:python:vector-multiplication)."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
