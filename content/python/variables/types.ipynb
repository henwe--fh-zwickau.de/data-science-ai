{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c3bc1182",
   "metadata": {},
   "source": [
    "(python:variables:types)=\n",
    "# Types\n",
    "\n",
    "Here we introduce two more very fundamental object types, discuss type conversion and introduce the Python-specific concept of immutability.\n",
    "\n",
    "## More Types\n",
    "\n",
    "In the [](python:crash) we met several data or object types (classes):\n",
    "* integers\n",
    "* floats\n",
    "* booleans\n",
    "* lists\n",
    "\n",
    "We also met strings, which will be discussed in more detail later on. Python ships with some more data types. Next to complex numbers, which we do not consider here, and several list-like types Python knows two very special data types:\n",
    "* None type\n",
    "* NotImplemented type\n",
    "\n",
    "Both types can represent only one value: `None` and `NotImplemented`, respectively. Thus, they can be considered as constants. But since in Python everything is an object, constants are objects, too. Objects have a type (class). Thus, there is a None type and a NotImplemented type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ec22f692",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'NoneType'>\n",
      "<class 'NotImplementedType'>\n"
     ]
    }
   ],
   "source": [
    "print(type(None))\n",
    "print(type(NotImplemented))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32776bd0",
   "metadata": {},
   "source": [
    "Existence of `NotImplemented` will be justified soon. Typically it's used as return value of functions to signal the caller that some expected functionality is not available.\n",
    "\n",
    "The value `None` is used whenever we want to express that a name is not tied to an object. In that case we simply tie the name to *the* `None` object. We write 'the' because the Python interpreter creates only one object of None type. Such an object can hold only one and the same value. So there is no reason to create several different `None` objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "1f6b044a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "94707183265600\n",
      "94707183265600\n"
     ]
    }
   ],
   "source": [
    "a = None\n",
    "b = None\n",
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "b1976440",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "139686343470576\n",
      "139686343473072\n"
     ]
    }
   ],
   "source": [
    "a = 'Some string'\n",
    "b = 'Some string'\n",
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f47fab6",
   "metadata": {},
   "source": [
    "In the second code block two string objects are created although both hold the same value. For both `None` values in the first code block only one object is created. If you play around with this you may find, that for short strings and small integers Python behaves like for `None`. This issue will be discussed in detail soon.\n",
    "\n",
    "```{hint}\n",
    "`None` is a Python keyword like `if` or `else` or `import`. It is used to refer to the object of None type. But the memory occupied by this object does not neccessarily contain a string 'None' or something similar. In fact, this object does not contain something useful. Its mileage is its existence, which allows to tie (temporarily unused) names to it. Same holds for `NotImplemented`.\n",
    "\n",
    "We already met this concept when introducing boolean values. `True` and `False` are Python keywords, too. They are used to refer to two different objects of type `bool`. But these objects do not contain a string 'True' or 'False' or something similar. Instead, a `bool` object stores an integer value: 1 for the `True` object and 0 for the `False` object. How to represent `None`, `True` and so on in memory depends on the concrete implementation of the Python interpreter and is not specified by the Python programming language.\n",
    "```\n",
    "\n",
    "## Type Casting\n",
    "\n",
    "Type casting means change of data type. An integer could be casted to a floating point number, for example. Python does not have a mechanism for type casting. Instead, dunder methods can be implemented to work with objects of different types.\n",
    "\n",
    "A very prominent dunder method for handling different object types is the `__init__` method, which is called after creating a new object. Its main purpose is to fill the new object with data. For Python standard types like `int`, `float`, `bool` the `__init__` method accepts several different data types as argument. \n",
    "\n",
    "We've already applied the function `int`, which creates an `int` object, to strings. Thus, we have seen that the `__init__` method of `int` objects accepts strings as argument and tries to convert them to an integer value. The other way round, `str` for creating string objects accepts integer arguments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "55dbb82b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'int'>\n",
      "123\n"
     ]
    }
   ],
   "source": [
    "a = '123'    # a string\n",
    "b = int(a)\n",
    "print(type(b))\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "6b9f3060",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'str'>\n",
      "123\n"
     ]
    }
   ],
   "source": [
    "a = 123    # an integer\n",
    "b = str(a)\n",
    "print(type(b))\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "b273132f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'float'>\n",
      "2.0\n"
     ]
    }
   ],
   "source": [
    "a = 2    # an integer\n",
    "b = float(a)\n",
    "print(type(b))\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0dafc99",
   "metadata": {},
   "source": [
    "Data may get lost due to type casting. The Python interpreter will **not** complain about possible data loss."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "51fc6f6d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'int'>\n",
      "2\n"
     ]
    }
   ],
   "source": [
    "a = 2.34    # a float\n",
    "b = int(a)\n",
    "print(type(b))\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68ba6a3e",
   "metadata": {},
   "source": [
    "```{hint}\n",
    "It's good coding style to use explicit type casting instead of relying on implicit conversions whenever this increases readability.\n",
    "\n",
    "A counter example is `1.23 * 56`, where the integer `56` is converted to float implicitely. Explicit casting would decrease readability: `1.23 * float(56)`.\n",
    "```\n",
    "\n",
    "```{note}\n",
    "If you define a custom object type, it depends on your implementation of the type's `__init__` method what data types can be cast to your type.\n",
    "```\n",
    "\n",
    "## Casting to Booleans\n",
    "\n",
    "Casting to `bool` maps 0, empty strings and similar values to `False`, all other values to `True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "59aa7569",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "True\n",
      "False\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "print(bool(None))\n",
    "print(bool(0))\n",
    "print(bool(123))\n",
    "print(bool(''))\n",
    "print(bool('hello'))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b295c19f",
   "metadata": {},
   "source": [
    "If we use non-boolean values where booleans are expected, Python implicitly casts to bool:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "2afa77f4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "cumbersome condition satisfied\n"
     ]
    }
   ],
   "source": [
    "if not '':\n",
    "    print('cumbersome condition satisfied')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00e31a78",
   "metadata": {},
   "source": [
    "For historical reasons boolean values internally are integers (0 or 1). This sometimes yields unexpected (but well-defined) results. An example is the comparison of integers to `True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "d932457c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "first if\n",
      "second else\n"
     ]
    }
   ],
   "source": [
    "a = 3\n",
    "\n",
    "if a:\n",
    "    print('first if')\n",
    "else:\n",
    "    print('first else')\n",
    "\n",
    "if a == True:\n",
    "    print('second if')\n",
    "else:\n",
    "    print('second else')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bcb4a137",
   "metadata": {},
   "source": [
    "The first condition is equivalent to `bool(3)`, which yields `True`, whereas the second is equivalent to `3 == 1`, yielding `False`. See [PEP 285](https://peps.python.org/pep-0285/#resolved-issues) for some discussion of that behavior (PEP 285 introduced `bool` to Python).\n",
    "\n",
    "## Immutability\n",
    "\n",
    "Objects in Python can be either *mutable* or *immutable*. Mutable objects allow modifying the value they hold. Immutable objects do not allow changing their values. Objects of simple type like `int`, `float`, `bool`, `str` are immutable whereas lists and most others are mutable.\n",
    "\n",
    "Understanding the concept of (im)mutability is fundamental for Python programming. Even if the source code suggests that an immutable object gets modified, a new object is created all the time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "2368e144",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "139686390710512\n",
      "139686390710544\n"
     ]
    }
   ],
   "source": [
    "a = 1\n",
    "print(id(a))\n",
    "\n",
    "a = a + 1\n",
    "print(id(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "576a7b16",
   "metadata": {},
   "source": [
    "This code snipped first creates an integer object holding the value 1 and then ties the name `a` to it. In line 3, sloppily speaking, `a` is increased by one. More precisely, a new integer object is created, holding the result 2 of the computation, and the name `a` is tied to this new object.\n",
    "\n",
    "Mutable objects behave as expected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "c95e8850",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "139686343563840\n",
      "139686343563840\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3]\n",
    "print(id(a))\n",
    "\n",
    "a[0] = 4\n",
    "print(id(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0ae63e7",
   "metadata": {},
   "source": [
    "Immutability of some data types allows the Python interpreter for more efficient operation and for code optimization during execution. We will discuss some of those efficiency related features later on.\n",
    "\n",
    "Always be aware of (im)mutability of your data. The following two code samples show fundamentally different behavior:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "b39a2581",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2 1\n"
     ]
    }
   ],
   "source": [
    "a = 1    # immutable integer\n",
    "b = a\n",
    "\n",
    "a = a + 1\n",
    "print(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1fb1ebc7",
   "metadata": {},
   "source": [
    "Increasing `a` does not touch `b`, because the integer object `1` `a` and `b` refer to is immutable. Increasing `a` creates a new object. Then `a` is tied to the new object and `b` still refers to the original one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "d66fbcae",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[4, 2, 3] [4, 2, 3]\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3]    # mutable list\n",
    "b = a\n",
    "\n",
    "a[0] = 4\n",
    "print(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29c8fbdd",
   "metadata": {},
   "source": [
    "Modifying `a` also modifies `b`, because `a` and `b` refer to the same mutable object.\n",
    "\n",
    "## Getting the Type\n",
    "\n",
    "Although rarely needed, we mention then built-in function `isinstance`. It takes an object and a type as parameters and returns `True` if the object is of the given type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "001534a7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "False\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "print(isinstance(8, int))\n",
    "print(isinstance(8, str))\n",
    "print(isinstance(8.0, float))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd518f81",
   "metadata": {},
   "source": [
    "## Useful Dunder Functions for Custom Types\n",
    "\n",
    "There a bunch of dunder functions one should implement when creating custom types (cf. [](python:crash:everything-object:custom-types)):\n",
    "* `__str__` is called by the Python interpreter to get a text representation of an object. For instance, it's called by `print` and whenever one tries to convert an object to string via `str(...)`.\n",
    "* `__repr__` is simlar to `__str__` but should return a more informative string representation. In the best case, it returns the Python code to recreate the object. See [Python's documentation](https://docs.python.org/3/reference/datamodel.html#object.__repr__) for details.\n",
    "* `__bool__` is called whenever an object has to be cast to `bool`.\n",
    "* `__len__` is called by the built-in function `len` to determin an object's length. This is useful for list-like objects.\n",
    "\n",
    "## Types are Objects\n",
    "\n",
    "Since everything in Python is an object, types are objects, too. Thus, types may provide member variables and methods in addition to the corresponding objects' member variables and methods. In some programming languages members of a type are called *static members*.\n",
    "\n",
    "Member variables of types occur for instance if constants have to be defined (almost always for convenience):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13247a3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "class ColorPair:\n",
    "    \n",
    "    red = (1, 0, 0)\n",
    "    green = (0, 1, 0)\n",
    "    blue = (0, 0, 1)\n",
    "    yellow = (1, 1, 0)\n",
    "    cyan = (0, 1, 1)\n",
    "    magenta = (1, 0, 1)\n",
    "    \n",
    "    def __init__(self, color1, color2):\n",
    "        self.color1 = color1\n",
    "        self.color2 = color2\n",
    "\n",
    "        \n",
    "my_pair = ColorPair(ColorPair.red, ColorPair.yellow)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9be708fe",
   "metadata": {},
   "source": [
    "Member functions of types are rarely used. One usecase are very flexible contructors for complex types, which do not fit into the `__init__` method due to many different variants of possible arguments. Often such constructors are named `from_...` and corresponding object creation code looks like\n",
    "```python\n",
    "my_object = SomeComplexType.from_other_type(arg1, arg2, arg3)\n",
    "```\n",
    "In such cases the `from_...` methods return a new object of corresponding type, that is, they implicitly call the `__init__` method.\n",
    "\n",
    "Defining methods for types requires advanced syntax contructs we do not discuss here."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
