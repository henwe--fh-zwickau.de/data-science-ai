{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "41e66271",
   "metadata": {},
   "source": [
    "(python:variables:names-objects)=\n",
    "# Names and Objects\n",
    "\n",
    "In contrast to most other programming languages Python follows a very clean and simple approach to memory access and management. We now dive deeper into Python's internal workings to come up with a new understanding of what we called *variables* in the [](python:crash).\n",
    "\n",
    "## Variables in the Non-Pythonian World\n",
    "\n",
    "Most programming languages, C for instance, assign fixed names to memory locations. Such combinations of memory location and name are known as variables. Assigning a value to a variable then means, that the compiler or interpreter writes the value to the memory location to which the variable’s name belongs. There is a one-to-one correspondence between variable names and memory locations.\n",
    "\n",
    "Consider the following C code:\n",
    "```C\n",
    "int a;\n",
    "int b;\n",
    "a = 5;\n",
    "b = a;\n",
    "```\n",
    "\n",
    "The first two lines tell the C compiler to reserve memory for two integer variables. The third line writes the value 5 to the location named `a`. The fourth line reads the value at the location named `a` and writes (copies) it to the location named `b`.\n",
    "\n",
    "```{figure} c-memory.svg\n",
    "---\n",
    "alt: scheme with sequence of memory locations and corresponding names\n",
    "scale: 70%\n",
    "---\n",
    "Memory is organized as a linear sequence of bytes. Used and currently unused bytes are managed by the operating system and by compiler. In C programs there is a one-to-one correspondence between variable names and memory locations.\n",
    "```\n",
    "\n",
    "## Variables in Python\n",
    "\n",
    "Python allows for multiple names per memory location and adds a layer of abstraction.\n",
    "\n",
    "In Python everything is an object and objects are stored somewhere in memory. If we use integers in Python, then the integer value is not written directly to memory. Instead, additional information is added and the resulting more complex data structure is written to memory.\n",
    "\n",
    "An newly created Python object does not have a name. Instead, Python internally assigns a unique number to each object, the object identifier or *object ID* for short. Thus, there is a one-to-one correspondence between object IDs and memory locations.\n",
    "\n",
    "In addition to a list of all object IDs (and corresponding memory locations), Python maintains a list of names occuring in the source code. Each name refers to exactly one object. But different names may refer to the same object. In this sense Python does not know variables as described above, but only objects and names tied to objects.\n",
    "\n",
    "Consider the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9ac8d2e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 5\n",
    "b = a"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c93c3951",
   "metadata": {},
   "source": [
    "The first line creates an integer object containing the value 5 and then ties the name `a` to this object. The second line takes the object referenced by the name `a` and ties a second name `b`to it.\n",
    "\n",
    "```{figure} python-memory.svg\n",
    "---\n",
    "alt: scheme with sequence of memory locations and corresponding object IDs and names\n",
    "scale: 70%\n",
    "---\n",
    "In Python one memory location may have several names, but a unique object ID.\n",
    "```\n",
    "\n",
    "```{important}\n",
    "Assignment operation `=` in Python is not about writing something to memory. Instead, Python takes the **existing** object on the right-hand side of `=` and ties an additional name to it.\n",
    "\n",
    "The object on the right-hand side may have existed before or it may be created by some operation specified by the code following `=`.\n",
    "\n",
    "It's also possible to create nameless objects. Simply omit `name =` before some object creation code.\n",
    "```\n",
    "\n",
    "Python has the built-in function `id` to get the ID of an object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "a9750d1d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "139710036296048\n",
      "139710036296048\n"
     ]
    }
   ],
   "source": [
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47c4278a",
   "metadata": {},
   "source": [
    "We see, that indeed `a` and `b` refer to the same object.\n",
    "\n",
    "Clear distinction between names and objects in Python adds flexibility, but also requires much more care when accessing or modifying data in memory. We will have to discuss possible pitfalls resulting from this concept at several points later on.\n",
    "\n",
    "## Equality of Objects\n",
    "\n",
    "In Python we have objects and we have values contained in the object. Thus, there are two fundamentally different questions which might be relevant for controlling program flow:\n",
    "* Do two names refer to the same object?\n",
    "* Do two objects (refered to by two names) contain the same value?\n",
    "\n",
    "Consider the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "4490b359",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1.23\n",
    "b = 1.23"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39e64623",
   "metadata": {},
   "source": [
    "It creates two `float` objects both holding the value 1.23. To see that there are two objects we can look at the object IDs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "518bbe65",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "139709972140912\n",
      "139709972145744\n"
     ]
    }
   ],
   "source": [
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc040e43",
   "metadata": {},
   "source": [
    "So the answer to the first question is 'no', but the answer to the second question is 'yes'.\n",
    "\n",
    "To compare equality of objects Python knows the `is` operator. To compare equality of values Python has the `==` operator. Both yield a boolean value as result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "391f3ecb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "print(a is b)\n",
    "print(a == b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c54c823",
   "metadata": {},
   "source": [
    "Negations of both operators are `is not` and `!=`, respectively. Using `is` is equivalent to comparing object IDs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "00c25224",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n"
     ]
    }
   ],
   "source": [
    "print(id(a) == id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e60a3e8",
   "metadata": {},
   "source": [
    "```{hint}\n",
    "Behavior of the `is` operator is hardwired in Python (use `==` on integer objects returned by `id`). But `==` simply calls the dunder method `__eq__` of the left-hand side object. Thus, what happens during comparison depends on an object's type. Writing your own classes (object types) you may implement the `__eq__` method whenever appropriate. Without custom implementation Python uses a default one behaving similarly to `is`.\n",
    "```\n",
    "\n",
    "## Local versus Global Names\n",
    "\n",
    "Names in Python have a scope, that is, a region of code where they are valid. Names defined outside functions and other structures are referred to as *global names* or *global variables* or simply *globals*. If a name is defined (that is, tied to some object) inside a function or some other structure, then the name is *local*. Local names are undefined outside the function or structure they are defined in."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "3067ab5b",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "123\n"
     ]
    },
    {
     "ename": "NameError",
     "evalue": "name 'd' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Input \u001b[0;32mIn [7]\u001b[0m, in \u001b[0;36m<cell line: 7>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      5\u001b[0m c \u001b[38;5;241m=\u001b[39m \u001b[38;5;241m123\u001b[39m\n\u001b[1;32m      6\u001b[0m my_func()\n\u001b[0;32m----> 7\u001b[0m \u001b[38;5;28mprint\u001b[39m(\u001b[43md\u001b[49m)\n",
      "\u001b[0;31mNameError\u001b[0m: name 'd' is not defined"
     ]
    }
   ],
   "source": [
    "def my_func():\n",
    "    print(c)\n",
    "    d = 456\n",
    "\n",
    "c = 123\n",
    "my_func()\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "129b9779",
   "metadata": {},
   "source": [
    "If there is a local name which is also a global name, than it's local version is used and the global one is left untouched."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "cc2b9d75",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "456\n",
      "123\n"
     ]
    }
   ],
   "source": [
    "def my_func():\n",
    "    c = 456\n",
    "    print(c)\n",
    "\n",
    "c = 123\n",
    "my_func()\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7938b584",
   "metadata": {},
   "source": [
    "But how to change a global variable from inside a function? The `global` keyword tells the interpreter that a name appearing in a function refers to a global variable. The interpreter then uses the global variable instead of creating a new local variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "d42937c4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "456\n",
      "456\n"
     ]
    }
   ],
   "source": [
    "def my_func():\n",
    "    global c\n",
    "    c = 456\n",
    "    print(c)\n",
    "\n",
    "c = 123\n",
    "my_func()\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f94dbdd1",
   "metadata": {},
   "source": [
    "We cannot access a global variable from inside a function and then introduce a local variable with the same name. This leads to an error because each name appearing in an assignment in a function is considered local throughout the function. Consequently, accessing the value of a global variable before creating a corresponding local variable is interpreted as accessing an undefined name. The interpreter then complains about accessing a local variable before assignment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "4e023418",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "UnboundLocalError",
     "evalue": "local variable 'c' referenced before assignment",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mUnboundLocalError\u001b[0m                         Traceback (most recent call last)",
      "Input \u001b[0;32mIn [10]\u001b[0m, in \u001b[0;36m<cell line: 6>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      3\u001b[0m     c \u001b[38;5;241m=\u001b[39m \u001b[38;5;241m456\u001b[39m\n\u001b[1;32m      5\u001b[0m c \u001b[38;5;241m=\u001b[39m \u001b[38;5;241m123\u001b[39m\n\u001b[0;32m----> 6\u001b[0m \u001b[43mmy_func\u001b[49m\u001b[43m(\u001b[49m\u001b[43m)\u001b[49m\n\u001b[1;32m      7\u001b[0m \u001b[38;5;28mprint\u001b[39m(c)\n",
      "Input \u001b[0;32mIn [10]\u001b[0m, in \u001b[0;36mmy_func\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[38;5;28;01mdef\u001b[39;00m \u001b[38;5;21mmy_func\u001b[39m():\n\u001b[0;32m----> 2\u001b[0m     \u001b[38;5;28mprint\u001b[39m(\u001b[43mc\u001b[49m)\n\u001b[1;32m      3\u001b[0m     c \u001b[38;5;241m=\u001b[39m \u001b[38;5;241m456\u001b[39m\n",
      "\u001b[0;31mUnboundLocalError\u001b[0m: local variable 'c' referenced before assignment"
     ]
    }
   ],
   "source": [
    "def my_func():\n",
    "    print(c)\n",
    "    c = 456\n",
    "\n",
    "c = 123\n",
    "my_func()\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77008079",
   "metadata": {},
   "source": [
    "```{important}\n",
    "It's considered bad practice to use lots of global variables. Global variables result in low readability of code. Exceptions prove the rule.\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
