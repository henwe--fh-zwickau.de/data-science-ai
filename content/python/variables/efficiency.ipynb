{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "27ff2622",
   "metadata": {},
   "source": [
    "(python:variables:efficiency)=\n",
    "# Efficiency\n",
    "\n",
    "Python’s combination of the names/objects concept and (im)mutability tends to waste memory and CPU time:\n",
    "* There might be many different objects holding all the same value. In principle, every time the number 1 occurs in the source code, a new integer object is created.\n",
    "* Tying names to other objects may leave objects without name. Such objects are no more accessible but resist in memory.\n",
    "* Modifying immutable objects requires to create new objects. Thus, even simple integer computations require relatively complex memory management operations.\n",
    "\n",
    "To mitigate these drawbacks, the Python interpreter uses several optimization strategies. Although such issues are rather technical we briefly discuss them here, because they sometimes yield unexpected results.\n",
    "\n",
    "## Preloaded Integers\n",
    "\n",
    "To avoid object creation every time a new integer is used, the Python interpreter pre-creates integer objects for all integers from -5 to 256. This saves CPU time. The somewhat cumbersome range stems from statistical considerations about integer usage.\n",
    "\n",
    "In addition, the interpreter takes care that no integer in this range is created twice during program execution. This saves memory. The behavior is demonstrated in the following code snipped:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9f063bc0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140556881723856\n",
      "140556881723856\n"
     ]
    }
   ],
   "source": [
    "a = 8\n",
    "b = 4 + 4\n",
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e03b0740",
   "metadata": {},
   "source": [
    "Both object IDs are identical, thus only one integer object is used. Since integer objects are immutable, this cannot cause any trouble.\n",
    "\n",
    "## String Interning\n",
    "\n",
    "As for integers, the Python interpreter tries to avoid multiple string objects with the same value. Since corresponding comparisons may require too much CPU time, this technique is only used for short strings. The rules controlling which strings get interned and which not are relatively complex."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "de6436d9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140556834482736\n",
      "140556834482736\n"
     ]
    }
   ],
   "source": [
    "a = 'short'\n",
    "b = 'sh' + 'ort'\n",
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "903d3acb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140556834581616\n",
      "140556834480368\n"
     ]
    }
   ],
   "source": [
    "a = 'very very long'\n",
    "b = 'very' + ' very long'\n",
    "print(id(a))\n",
    "print(id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da7ec6c6",
   "metadata": {},
   "source": [
    "## Repeated Literals in Source Code\n",
    "\n",
    "Before executing a Python program, the interpreter checks the syntax and creates a list of all literals. Here, literals are all types of explicit data appearing in the source code, like integers or strings. If some literal appears multiple times and if objects of the corresponding data type are immutable, only one object is created.\n",
    "\n",
    "```\n",
    "# Copy the following Python code to a text file and feed the file to the\n",
    "# Python interpreter to see the effect of optimization of repreated literals.\n",
    "\n",
    "a = 'a long string, which usually is not interned'\n",
    "b = 12345678\n",
    "c = 'a long string, which usually is not interned'\n",
    "d = 12345678\n",
    "\n",
    "print(id(a))\n",
    "print(id(b))\n",
    "print(id(c))\n",
    "print(id(b))\n",
    "```\n",
    "\n",
    "The names `a` and `c` will point to the same string object, although the string is too long to be interned by the string interning mechanism. The names `b` and `d` will point to the same integer object, although they are outside the range of preloaded integers.\n",
    "\n",
    "Care has to be taken when using interactive Python interpreters like Jupyter. If the above code snipped is executed line by line in an interactive interpreter, then four different objects will be created, because the interpreter does not parse the full code in advance.\n",
    "\n",
    "```{important}\n",
    "Executing Python code with an interactive interpreter may yield different results than executing the same code at once with a non-interactive interpreter! In particular, performance measures likes memory consumption may differ.\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "b79ec4af",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140556836199504\n",
      "140556834411568\n",
      "140556836199600\n",
      "140556834416144\n"
     ]
    }
   ],
   "source": [
    "a = 'a long string, which usually is not interned'\n",
    "b = 12345678\n",
    "c = 'a long string, which usually is not interned'\n",
    "d = 12345678\n",
    "\n",
    "print(id(a))\n",
    "print(id(b))\n",
    "print(id(c))\n",
    "print(id(d))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7f80e7c",
   "metadata": {},
   "source": [
    "(python:variables:efficiency:garbage-collection)=\n",
    "## Garbage Collection\n",
    "\n",
    "As described above, there might be objects without names. Such objects resist in memory, but are no more accessible. To avoid filling up memory as time passes, the Python interpreter automatically removes nameless objects from memory. This mechanism is known as garbage collection and is a feature not available in all programming languages.\n",
    "In the C programming language, for instance, the programmer has to take care to free memory, if data isn't needed anymore.\n",
    "\n",
    "Sometimes, especially when working with large data sets, one wants to get rid of some data in memory to have more memory available for other purposes. One way is to tie all names refering to the no more needed object to other objects, which is somewhat unintuitive. Alternatively, the `del` keyword can be used to untie a name from an object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "3724aedf",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'a' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Input \u001b[0;32mIn [5]\u001b[0m, in \u001b[0;36m<cell line: 3>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m a \u001b[38;5;241m=\u001b[39m \u001b[38;5;241m5000\u001b[39m\n\u001b[1;32m      2\u001b[0m \u001b[38;5;28;01mdel\u001b[39;00m a\n\u001b[0;32m----> 3\u001b[0m \u001b[38;5;28mprint\u001b[39m(\u001b[43ma\u001b[49m)\n",
      "\u001b[0;31mNameError\u001b[0m: name 'a' is not defined"
     ]
    }
   ],
   "source": [
    "a = 5000\n",
    "del a\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4602a50",
   "metadata": {},
   "source": [
    "The last line leads to an error message, because after executing line 2 the name `a` is no more valid.\n",
    "Note that `del` only deletes the name, not the object. In the following code snipped the object remains in memory, because it has another name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "a99a2205",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5000\n"
     ]
    }
   ],
   "source": [
    "a = 5000\n",
    "b = a\n",
    "del a\n",
    "print(b)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
