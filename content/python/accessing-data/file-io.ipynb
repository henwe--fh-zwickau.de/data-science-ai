{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "be9ac75d",
   "metadata": {},
   "source": [
    "(python:accessing-data:file-io)=\n",
    "# File IO\n",
    "\n",
    "Next to screen IO, input from and output to files is the most basic operation related to data processing. Almost all data science projects start with reading data from one or more files. In this chapter we discuss basic file access. Later on there will be several specialized modules and functions to make things more straight forward. But from time to time, in case of uncommon file formats, one has to resort to the most basic operations.\n",
    "\n",
    "## Basics\n",
    "\n",
    "Reading data from a file or writing data to a file requires three steps:\n",
    "\n",
    "````{card}\n",
    "**1. Open the file**\n",
    "^^^\n",
    "Tell the operating system, that file access is required. The operating system checks permissions and, if everything is okay, returns a file identifier (usually a number), which has to be used for all subsequent file operations.\n",
    "````\n",
    "\n",
    "````{card}\n",
    "**2. Read or write data**\n",
    "^^^\n",
    "Tell the operating system to move data between the file and some place in memory which can be accessed by the Python interpreter.\n",
    "````\n",
    "\n",
    "````{card}\n",
    "**3. Close the file**\n",
    "^^^\n",
    "Tells the operating system, that file access is no longer required. The operating system, thus, knows that other applications now may read from or write to the file.\n",
    "````\n",
    "\n",
    "In Python all file related data and operations are encapsulated into a file object. There are different types of file objects depending on the file type (text or binary) and on some technical issues. All types of file objects provide identical member functions for reading and writing. Here is the basic procedure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "3da090ef",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Some text\n",
      "in some file\n",
      "splitted over\n",
      "multiple lines.\n"
     ]
    }
   ],
   "source": [
    "f = open('testdir/testfile.txt', 'r')\n",
    "file_content = f.read()\n",
    "f.close()\n",
    "\n",
    "print(file_content)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b1c2c93",
   "metadata": {},
   "source": [
    "This code snipped opens a file for reading (argument `'r'`) and assignes the name `f` to the resulting file object. Then the whole content of the file is stored in the string object `file_content`. Finally, the file is closed and it's content is printed to screen.\n",
    "\n",
    "If something goes wrong, for instance the file does not exist, the Python interpreter stops execution with an error message. For the moment, we do not do any error checking when operating with files (this is very bad practice!).\n",
    "\n",
    "```{note}\n",
    "The `read` method and all other methods for reading and writing files can be used to process text data and binary data. Providing the `'r'` argument to `open` tells Python to open the file as text file. Reading data from the file results in a string object.\n",
    "If '`rb'` is used instead, then the file is handled as binary file and reading results in a list of bytes.\n",
    "Details will be discussed in the chapter on [](python:accessing-data:text-files).\n",
    "\n",
    "Default mode is `'r'`. So specifying no mode opens for reading in text mode.\n",
    "```\n",
    "\n",
    "Important methods for reading and writing files are `read`, `readline`, `readlines`, `write`, `writelines`, `seek`. See [methods of file objects](https://docs.python.org/3/tutorial/inputoutput.html#methods-of-file-objects) in the Python documentation.\n",
    "\n",
    "For more details on access modes see [documention of `open`](https://docs.python.org/3/library/functions.html#open).\n",
    "\n",
    "## Paths are OS Dependent\n",
    "\n",
    "Paths to a file are operating system dependent. Thus, using paths in the `open` function makes our code operating system dependent. This should be avoided and luckily there are techniques to avoid such OS dependence.\n",
    "\n",
    "::::{grid}\n",
    "\n",
    ":::{grid-item-card}\n",
    "**Linux/Unix/macOS**\n",
    "^^^\n",
    "In Linux and other Unix like systems (macOS for instance), all files can be accessed via paths of the form `‘/directory/subdirs/file’`. That is, a list of directory names separated by slashs and ending with the file name. If the path starts with a slash, then it's an absolute path, else a relative one.\n",
    "\n",
    "Drives can be mounted as directory everythere in the file system’s hierarchy. Thus, there is no need for special drive related path components.\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    "**Windows**\n",
    "^^^\n",
    "Windows uses a different format: `'drive:\\directory\\subdirs\\file'`. Instead of slashs backslashs are used as delimiters and there is an additional drive letter in absolute paths followed by a colon. The purpose of the drive letter is to select one of several physical (or even logical) drives.\n",
    ":::\n",
    "::::\n",
    "\n",
    "From the programmer’s point of view, additional effort is required to make code work in both worlds.\n",
    "\n",
    "## OS Independent Paths in Python\n",
    "\n",
    "The Python module `os.path` provides the function `join`. This function takes directory names and a file name as arguments and returns a string containing the corresponding path with appropriate (OS dependent) delimiters. So output of the follow code snipped depends on the OS used for execution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "3a0ca054",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "testdir/testfile.txt\n"
     ]
    }
   ],
   "source": [
    "import os.path\n",
    "\n",
    "test_path = os.path.join('testdir', 'testfile.txt')\n",
    "\n",
    "print(test_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7eccf913",
   "metadata": {},
   "source": [
    "The path separator (`/` or `\\`) used by the OS is available in `os.sep`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5fad0d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "print('path separator:', os.sep)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33c75a35",
   "metadata": {},
   "source": [
    "## Directory Listings\n",
    "\n",
    "Often data sets are scattered over many files, for instance one file per customer, each file containing all the customers transactions in an online shop. In such cases we need to get a list of all files in a specified directory. Such functionality is provided by Python's `glob` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "8bc52c69",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "testdir/test.zip\n",
      "testdir/testwrite-windows.txt\n",
      "testdir/iso8859-1.txt\n",
      "testdir/umlauts.txt\n",
      "testdir/testfile.txt\n",
      "testdir/testwrite.txt\n",
      "testdir/utf-8.txt\n"
     ]
    }
   ],
   "source": [
    "import glob\n",
    "\n",
    "file_list = glob.glob('testdir/*')\n",
    "\n",
    "for file in file_list:\n",
    "    print(file)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "064670cd",
   "metadata": {},
   "source": [
    "The `glob` module's `glob` function takes a path containing wildcards like `*` (arbitrary string) and `?` (arbitrary character), for instance, and returns a list of all files matching the specified path.\n",
    "\n",
    "```{note}\n",
    "To make above code snipped OS independent we should write `glob.glob(os.path.join('testdir'), '*'))`.\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
