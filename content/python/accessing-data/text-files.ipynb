{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "917e18d1",
   "metadata": {},
   "source": [
    "(python:accessing-data:text-files)=\n",
    "# Text Files\n",
    "\n",
    "Text files are the most basic type of files. They contain string data. Historically there was a one-to-one mapping between byte values (0...255) and characters. Nowadays things are much more complex, because representing all the world's languages requires more than 256 different characters. When reading from and writing to text files the mapping between characters and there numerical representation in memory or storage devices is of uttermost importance.\n",
    "\n",
    "Text file not only contain so called pritnable characters like letters and numbers, but also control characters like line breaks and tab stops. Related issues will be discussed in this chapter, too.\n",
    "\n",
    "## Encodings\n",
    "\n",
    "Every kind of data has to be converted to a stream of bits. Else it cannot be processed by a computer. For strings we have to distinguish between their representation on screen (which symbol) and their representation in memory (which sequence of bits). Mapping between screen and memory representation is known as *encoding*. *Decoding* is mapping in opposite direction.\n",
    "\n",
    "```{figure} xkcd927.png\n",
    "---\n",
    "alt: line drawing showing two persons discussing about the need of a 15th unifying standard already given 14\n",
    "---\n",
    "Fortunately, the charging one has been solved now that we've all standardized on mini-USB. Or is it micro-USB? Shit. Source: Randall Munroe, [xkcd.com/927](https://xkcd.com/927)\n",
    "```\n",
    "\n",
    "### ASCII\n",
    "\n",
    "Historically, each character of a string has been encoded as exactly one byte. A byte can hold values from 0 to 255. Thus, only 256 different characters are available, including so called control characters like tabs and new line characters.\n",
    "\n",
    "The mapping between byte values and characters, the so called *character encoding*, has to be standardized to allow exchanging text files. For a long time, the most widespread standard has been *ASCII* (American Standard Code for Information Interchange). But since ASCII does not contain special characters like umlauts in other languages, several other encodings were developed. The [ISO 8859](https://en.wikipedia.org/wiki/ISO/IEC_8859) family is a very prominent set of ASCII derivates.\n",
    "\n",
    "The first 128 characters of almost all encodings coincide with ASCII, but the remaining 128 contain different symbols. Thus, to read text files one has to know the encoding used for saving the file. Typically, the encoding is not (!) saved in the file, but has to be guessed or communicated along with the file. Have a look at the [list of encodings](https://docs.python.org/3/library/codecs.html#standard-encodings) Python can process.\n",
    "\n",
    "### Unicode\n",
    "\n",
    "Nowadays, *Unicode* is the standard encoding. More precisely, Unicode defines a group of encodings. We do not go into the details here. For our purposes it suffices to know that Unicode contains several hundred thousand symbols and the most important encoding of Unicode is called *UTF-8*. The eight means that most characters require only 8 bits. The symbols associated with the byte values 0 to 127 coincide with ASCII. A byte value above 127 indicates a multi-byte symbol comprising two, three, or four bytes.\n",
    "\n",
    "::::{grid}\n",
    "\n",
    ":::{grid-item-card}\n",
    "**Linux/Unix/macOS**\n",
    "^^^\n",
    "Non-Windows systems (Linux, Unix, macOS) have native UTF-8 support for decades. It's the standard encoding for Websites and other internet related applications.\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    "**Windows**\n",
    "^^^\n",
    "Windows, even Windows 10, uses a different Unicode encoding under the hood and supports UTF-8 at the surface only. Sometimes, if one has to dig deeper into the system, unexpected things may happen. Older Windows version did not have UTF-8 support at all. Always check the encoding if you work with text data generated on a Windows system!\n",
    ":::\n",
    "::::\n",
    "\n",
    "### Encodings in Python\n",
    "\n",
    "Python uses UTF-8 and strictly distinguishs between strings and their encoded representation. The string is what we see on screen, whereas the encoded form is what is written to memory and storage devices.\n",
    "\n",
    "String objects provide the `encode` member function. This function returns a sequence of bytes. This sequence is of type `bytes`. A `bytes` object is immutable. In essence, it's a tuple of integers between 0 and 255.\n",
    "\n",
    "The other way round `bytes` objects provide a member function `decode` to transform them to strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "7ab7ac1f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "b'some string with umlauts: \\xc3\\xa4, \\xc3\\xb6, \\xc3\\xbc'\n"
     ]
    }
   ],
   "source": [
    "a = 'some string with umlauts: ä, ö, ü'\n",
    "b = a.encode()\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "467d4612",
   "metadata": {},
   "source": [
    "As we see, `bytes` objects can be specified like strings, but prefixed by `b`. The only difference is that all bytes holding values above 127 or non-printable characters (line breaks, for instance) are replaced by their integer values in hexadecimal notation with the prefix `\\x`, which is the escape sequence for specifying characters in hexadecimal notation. If we want to use octal notation, the escape sequence is `\\000` where `000` is to be replaced by a three digit octal number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "7668f425",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "some string with umlauts: ä, ö, ü\n"
     ]
    }
   ],
   "source": [
    "c = b.decode()\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec6387ea",
   "metadata": {},
   "source": [
    "```{note}\n",
    "The `encode` and `decode` methods accept an optional `encoding` parameter, which defaults to `'utf-8'`.\n",
    "```\n",
    "\n",
    "There is also a mutable version of `bytes` objects: `bytearray` objects. They provide a `decode` function, too.\n",
    "\n",
    "Reading from a file opened in text mode is equivalent to reading after opening in binary mode followed by a call to `decode`. Similarly for writing. The `open` function has knowns an optional `encoding` parameter for text mode, defaulting to `'utf-8'`.\n",
    "\n",
    "## Line Breaks\n",
    "\n",
    "Encoding line breaks in text files is done differently on different operating systems. The ASCII and Unicode standards define two symbols indicating a line break. One is symbol 10, known as line feed (LF for short). The other is symbol 13, known as carriage return (CR for short).\n",
    "\n",
    "Historically, when typewriters were the standard text processing tools, starting a new line required two actions: move to next line without moving the carriage, then move the carriage to its rightmost position. Thus, there are two different symbols for these two actions.\n",
    "\n",
    "::::{grid}\n",
    "\n",
    ":::{grid-item-card}\n",
    "**Linux/Unix/macOS**\n",
    "^^^\n",
    "Linux and other Unix like system (macOS, for instance) use single byte line breaks encoded by LF. Old versions of macOS used CR, but then developers switched to LF.\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    "**Windows**\n",
    "^^^\n",
    "Windows adhers to the two-step legacy from pre-computer era. That is, on Windows line breaks in text data are encoded by the two bytes CR and LF.\n",
    ":::\n",
    "::::\n",
    "\n",
    "Python can handle all three versions of line break codes (LF, CR, CR LF) and tries to hide the differences from the programmer. But be aware, that writing text files may produce different results on Windows and Linux/Unix/macOS machines.\n",
    "\n",
    "## Encoding Problem Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ae7e9806",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os.path"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0639efbb",
   "metadata": {},
   "source": [
    "### Wrong Encoding\n",
    "\n",
    "If we open an ISO 8859-1 encoded text file without specifying an encoding (that is, UTF-8 is used), the interpreter fails either fails to interpret some bytes or it shows wrong symbols."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "918bf909",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "UnicodeDecodeError",
     "evalue": "'utf-8' codec can't decode byte 0xe4 in position 24: invalid continuation byte",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mUnicodeDecodeError\u001b[0m                        Traceback (most recent call last)",
      "Input \u001b[0;32mIn [4]\u001b[0m, in \u001b[0;36m<cell line: 2>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m f \u001b[38;5;241m=\u001b[39m \u001b[38;5;28mopen\u001b[39m(os\u001b[38;5;241m.\u001b[39mpath\u001b[38;5;241m.\u001b[39mjoin(\u001b[38;5;124m'\u001b[39m\u001b[38;5;124mtestdir\u001b[39m\u001b[38;5;124m'\u001b[39m, \u001b[38;5;124m'\u001b[39m\u001b[38;5;124miso8859-1.txt\u001b[39m\u001b[38;5;124m'\u001b[39m), \u001b[38;5;124m'\u001b[39m\u001b[38;5;124mr\u001b[39m\u001b[38;5;124m'\u001b[39m)\n\u001b[0;32m----> 2\u001b[0m text \u001b[38;5;241m=\u001b[39m \u001b[43mf\u001b[49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43mread\u001b[49m\u001b[43m(\u001b[49m\u001b[43m)\u001b[49m\n\u001b[1;32m      3\u001b[0m f\u001b[38;5;241m.\u001b[39mclose()\n\u001b[1;32m      5\u001b[0m \u001b[38;5;28mprint\u001b[39m(text)\n",
      "File \u001b[0;32m~/anaconda3/envs/ds_book/lib/python3.10/codecs.py:322\u001b[0m, in \u001b[0;36mBufferedIncrementalDecoder.decode\u001b[0;34m(self, input, final)\u001b[0m\n\u001b[1;32m    319\u001b[0m \u001b[38;5;28;01mdef\u001b[39;00m \u001b[38;5;21mdecode\u001b[39m(\u001b[38;5;28mself\u001b[39m, \u001b[38;5;28minput\u001b[39m, final\u001b[38;5;241m=\u001b[39m\u001b[38;5;28;01mFalse\u001b[39;00m):\n\u001b[1;32m    320\u001b[0m     \u001b[38;5;66;03m# decode input (taking the buffer into account)\u001b[39;00m\n\u001b[1;32m    321\u001b[0m     data \u001b[38;5;241m=\u001b[39m \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39mbuffer \u001b[38;5;241m+\u001b[39m \u001b[38;5;28minput\u001b[39m\n\u001b[0;32m--> 322\u001b[0m     (result, consumed) \u001b[38;5;241m=\u001b[39m \u001b[38;5;28;43mself\u001b[39;49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43m_buffer_decode\u001b[49m\u001b[43m(\u001b[49m\u001b[43mdata\u001b[49m\u001b[43m,\u001b[49m\u001b[43m \u001b[49m\u001b[38;5;28;43mself\u001b[39;49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43merrors\u001b[49m\u001b[43m,\u001b[49m\u001b[43m \u001b[49m\u001b[43mfinal\u001b[49m\u001b[43m)\u001b[49m\n\u001b[1;32m    323\u001b[0m     \u001b[38;5;66;03m# keep undecoded input until the next call\u001b[39;00m\n\u001b[1;32m    324\u001b[0m     \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39mbuffer \u001b[38;5;241m=\u001b[39m data[consumed:]\n",
      "\u001b[0;31mUnicodeDecodeError\u001b[0m: 'utf-8' codec can't decode byte 0xe4 in position 24: invalid continuation byte"
     ]
    }
   ],
   "source": [
    "f = open(os.path.join('testdir', 'iso8859-1.txt'), 'r')\n",
    "text = f.read()\n",
    "f.close()\n",
    "\n",
    "print(text)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c6be0c0",
   "metadata": {},
   "source": [
    "If we open an UTF-8 encoded file with ISO 8859-1 decoding we see garbled symbols."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "79c63620",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Some umlauts: Ã¤, Ã¶, Ã¼.\n",
      "This file is UTF-8 encoded.\n"
     ]
    }
   ],
   "source": [
    "f = open(os.path.join('testdir', 'utf-8.txt'), 'r', encoding='iso-8859-1')\n",
    "text = f.read()\n",
    "f.close()\n",
    "\n",
    "print(text)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9305a8cd",
   "metadata": {},
   "source": [
    "### Writing Line Breaks\n",
    "\n",
    "The following code produces different files on Linux/Unix/macOS and Windows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "18fa44cd",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "f = open(os.path.join('testdir', 'testwrite.txt'), 'w')\n",
    "text = f.write('test\\n\\n\\n\\n\\n\\n\\n\\n\\n\\ntest')\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2eada63",
   "metadata": {},
   "source": [
    "On Linux and Co. the file will have 18 bytes. On Windows it will have 28 bytes due to Windows' 2-byte line breaks. Opening the file in binary mode shows the line break encoding:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "cfca62d8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(116, 101, 115, 116, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 116, 101, 115, 116)\n"
     ]
    }
   ],
   "source": [
    "f = open(os.path.join('testdir', 'testwrite.txt'), 'rb')\n",
    "text = f.read()\n",
    "f.close()\n",
    "\n",
    "print(tuple(text))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce1076f0",
   "metadata": {},
   "source": [
    "Using `print(text)` directly shows line breaks as `\\n`, which is nice almost always, but not here. So we convert the bytes object to a tuple of integers before printing.\n",
    "\n",
    "If the file has been writen on a Windows machine, it looks like that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "b0b2f33f",
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(116, 101, 115, 116, 13, 10, 13, 10, 13, 10, 13, 10, 13, 10, 13, 10, 13, 10, 13, 10, 13, 10, 13, 10, 116, 101, 115, 116)\n"
     ]
    }
   ],
   "source": [
    "f = open(os.path.join('testdir', 'testwrite-windows.txt'), 'rb')\n",
    "text = f.read()\n",
    "f.close()\n",
    "\n",
    "print(tuple(text))"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
