{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "44a91f48",
   "metadata": {},
   "source": [
    "(python:error-handling-debugging-overview)=\n",
    "# Error Handling and Debugging Overview\n",
    "\n",
    "Up to now we did not care about error handling. If something went wrong, the Python interpreter stopped execution and printed some message. But Python provides techniques for more controlled error handling.\n",
    "\n",
    "## Error Handling\n",
    "\n",
    "### Syntax Versus Runtime Errors\n",
    "\n",
    "The Python interpreter parses the whole souce code file before execution.\n",
    "In this phase the interpreter may encounter *syntax errors*.\n",
    "That is, the interpreter does not understand what we want him to do. The code does not look like Python code should look like.\n",
    "Syntax errors are easily recovered by the programmer.\n",
    "\n",
    "The more serious types of errors are *runtime errors* (or *semantic errors*) which occur during program execution.\n",
    "Handling runtime errors is sometimes rather difficult.\n",
    "\n",
    "### Handling Runtime Errors\n",
    "\n",
    "The traditional way for handling runtime errors is to avoid runtime errors at all. All user input and all other sources of possible trouble get checked in advance by incorporating suitable `if` clauses in the code.\n",
    "This approach decreases readability of code, because the important lines are hidden between lots of error checking routines.\n",
    "\n",
    "The more pythonic way of handling runtime errors are *exceptions*.\n",
    "Everytime the interpreter encounters some problem, like division by zero, it *throws an exception*.\n",
    "The programmer may *catch the exception* and handle it appropriately or the programmer may leave exception handling to the Python interpreter.\n",
    "In the latter case, the interpreter usually stops execution and prints a detailed error message.\n",
    "\n",
    "### Basic Exception Handling Syntax\n",
    "\n",
    "Here is the basic syntax for catching and handling exceptions:\n",
    "```python\n",
    "try:\n",
    "    # code which may cause troubles\n",
    "except ExceptionName:\n",
    "    # code for handling a certain exception caused by code in try block\n",
    "except AnotherExceptionName:\n",
    "    # code for handling a certain exception caused by code in try block\n",
    "else:\n",
    "    # code to execute after successfully finishing try block\n",
    "```\n",
    "\n",
    "The `try` block contains the code to be protected, that is, the code which might raise an exception.\n",
    "Then there is at least one `except` block. The code in the `except` block is only executed, if the specified exception has been raised.\n",
    "In this case, execution of the `try` block is stopped immediately and execution continues in the `except` block.\n",
    "\n",
    "There can be several `except` blocks for handling different types of exceptions.\n",
    "Instead of an exception name also a tuple of names can be given to handle several different exceptions in one block.\n",
    "\n",
    "The `else` block is executed after successfully finishing the `try` block, that is, if no exception occurred. \n",
    "Here is the right place for code which shall only be executed if no exception occurred, but for which no explicit exception handling shall be implemented.\n",
    "\n",
    "Here is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "fd27f0a2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Division by zero. Setting result to 1000.\n",
      "Result is 1000.\n"
     ]
    }
   ],
   "source": [
    "a = 0    # some number from somewhere (e.g., user input)\n",
    "\n",
    "try:\n",
    "    b = 1 / a\n",
    "except ZeroDivisionError:\n",
    "    print('Division by zero. Setting result to 1000.')\n",
    "    b = 1000    # set b to some (reasonable) value\n",
    "else:\n",
    "    print('Everything okay.')\n",
    "\n",
    "print('Result is {}.'.format(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7483517",
   "metadata": {},
   "source": [
    "Without using exception handling the interpreter would stop execution in the division line.\n",
    "By catching the exception we can avoid this automatic behavior and handle the problem in a way which does not prevent further program execution.\n",
    "\n",
    "Note that exception names are not strings, but names of object types (classes).\n",
    "Thus, don't use quotation markes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "592a4ad4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'type'>\n",
      "['__cause__', '__class__', '__context__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__setstate__', '__sizeof__', '__str__', '__subclasshook__', '__suppress_context__', '__traceback__', 'args', 'with_traceback']\n"
     ]
    }
   ],
   "source": [
    "print(type(ZeroDivisionError))\n",
    "print(dir(ZeroDivisionError))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d59f87ae",
   "metadata": {},
   "source": [
    "The Python documentation contains a [list of built-in exceptions](https://docs.python.org/3/library/exceptions.html#concrete-exceptions). There is a kind of tree structure in the set of all exceptions and we may define new exceptions if we need them to express errors specific to our program. These topics will be discussed in detail when delving deeper into object oriented programming.\n",
    "\n",
    "### Clean-Up\n",
    "\n",
    "Sometimes it's necessary to do some clean-up operations like closing a file no matter an exception occurred or not while keeping the file open fro reading and writing.\n",
    "For this purpose Python provides the `finally` keyword:\n",
    "```python\n",
    "try:\n",
    "    # code which may cause troubles\n",
    "except ExceptionName:\n",
    "    # code for handling a certain exception caused by code in try block\n",
    "else:\n",
    "    # code to execute after successfully finishing try block\n",
    "finally:\n",
    "    # code for clean-up operations\n",
    "```\n",
    "\n",
    "The `finally` block is executed after the `try` block if no exception occured.\n",
    "If an exception occurred, then the `finally` block is executed after the corresponding `except` clause.\n",
    "If `try` or `except` clauses contain `break`, `continue` or `return`, then the `finally` block is executed *before* `break`, `continue` or `return`, respectively.\n",
    "If a `finally` block executed before `return` contains a `return` itself, then `finally`'s `return` is used and the original `return` is ignored.\n",
    "\n",
    "```{note}\n",
    "As long as a file is opened by our program the operating system blocks file access for other programs. Thus, we should close a file as soon as possible. Forgetting to close a file is not too bad because the OS will close it for use after program execution stopped. But for long running programs with only short file access as start-up a non-closed file may block access by other programs for hours or days. Thus, always, especially in case of exception handling, make sure that in each situation (with or without exception) files get closed properly by the program.\n",
    "```\n",
    "\n",
    "### Objects With Predefined Clean-Up Actions\n",
    "\n",
    "Some object types, file objects for instance, include predefined clean-up actions.\n",
    "That is, for certain operations (e.g., opening a file) they define what should be done in a corresponding `finally` block (e.g., closing the file), if the operations would be placed in a `try` block.\n",
    "\n",
    "To use this feature Python has the `with` keyword:\n",
    "```python\n",
    "with open('some_file') as f:\n",
    "    # do something with file object f\n",
    "```\n",
    "\n",
    "If the `open` function is successful, then the indented code block is executed.\n",
    "If `open` fails, an exception is raised.\n",
    "In both cases, `with` ensures, that proper clean-up (closing the file) takes place.\n",
    "\n",
    "Objects which can be used with `with` are said to support the *context management protocol*.\n",
    "Such objects can also be defined by the programmer using dunder methods, see [Python's documentation](https://docs.python.org/3/library/stdtypes.html#context-manager-types) for details.\n",
    "\n",
    "The purpose of `with` is to make code more readable by avoiding too many `try...except...finally` blocks.\n",
    "\n",
    "## Logging and Debugging\n",
    "\n",
    "Up to now we considered syntax errors, which basically are typos in the code, and semantic errors, which are caused by unexpected user input or failed file access. But code may contain more involved semantic errors, which may be hard to identify. The process of finding and correcting semantic errors is known as *debugging*.\n",
    "\n",
    "A simple approach to debugging is to print status information during program flow. For private scripts and data scientist's everyday use this suffices.\n",
    "For higher quality programs the Python standard library provides the `logging` package, which allows to redirect some of the status information to a *log file*.\n",
    "Logging basics are described in the [basic logging tutorial](https://docs.python.org/3/howto/logging.html#basic-logging-tutorial).\n",
    "\n",
    "If looking at log messages does not suffice, there are programs specialized to debugging your code. We do not cover this topic here. But if you are interested in you should have a look at [The Python Debugger](https://docs.python.org/3/library/pdb.html) and at [Debugging with Spyder](https://docs.spyder-ide.org/debugging.html).\n",
    "\n",
    "## Profiling\n",
    "\n",
    "Sometimes our code does what you want it to do, but it is too slow or consumes too much memory (out of memory error from the operating system). Then it's time for profiling.\n",
    "\n",
    "You may use the [Spyder Profiler](https://docs.spyder-ide.org/profiler.html) or import profiling functionality from suitable Python packages.\n",
    "\n",
    "### Profiling Execution Time\n",
    "\n",
    "The `timeit` module provides tools for measuring a Python script's execution time in seconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "8e5f4059",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.057730700998945395\n"
     ]
    }
   ],
   "source": [
    "import timeit\n",
    "\n",
    "a = 1.23\n",
    "\n",
    "code = \"\"\"\\\n",
    "b = 4.56 * a\n",
    "\"\"\"\n",
    "\n",
    "print(timeit.timeit(stmt=code, number=1000000, globals=globals()))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3422ac2c",
   "metadata": {},
   "source": [
    "This code snipped packs some code into the string `code` and passes it to the `timeit` function.\n",
    "This function executes the code `number` times to increase accuracy.\n",
    "The built-in function `globals` returns a list of all defined names.\n",
    "This list should be passed to the `timeit` function to provide access to all names.\n",
    "\n",
    "Have a look at the [The Python Profilers](https://docs.python.org/3/library/profile.html), too.\n",
    "\n",
    "```{note}\n",
    "If working in Jupyter you may use the [`%timeit`](https://ipython.readthedocs.io/en/stable/interactive/magics.html#magic-timeit) magic instead of the `timeit` module.\n",
    "```\n",
    "\n",
    "### Profiling Memory Consumption\n",
    "\n",
    "From data science view also memory consumption is of interest, because handling large data sets requires lots of memory.\n",
    "There are many ways to obtain memory information. A simple one is as follows (install module `pympler` first):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "869147dd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "72\n",
      "32\n"
     ]
    }
   ],
   "source": [
    "from pympler import asizeof\n",
    "\n",
    "my_string = 'This is a string.'\n",
    "my_int = 23\n",
    "\n",
    "print(asizeof.asizeof(my_string))\n",
    "print(asizeof.asizeof(my_int))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a8a4b46",
   "metadata": {},
   "source": [
    "This gives the size of the memory allocated for some object.\n",
    "This number also includes the size of 'subobjects', that is, for example, all the objects referenced by a list object are included."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
