{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "18c65104",
   "metadata": {},
   "source": [
    "(python:functions:passing-arguments)=\n",
    "# Passing Arguments\n",
    "\n",
    "In contrast to several other programming languages Python provides very flexible and readable syntax constructs for passing data to functions. Here we'll also discuss what happens in memory when passing data to functions.\n",
    "\n",
    "## Positional Arguments\n",
    "\n",
    "Positional arguments have to be passed in exactly the same order as they appear in the function's definition.\n",
    "There can be as many positional arguments as needed. But a function may come without any positional arguments at all, too.\n",
    "\n",
    "Positional arguments may have a default value, which is used if the argument is missing in a function call. Syntax:\n",
    "```python\n",
    "def my_function(arg1=default_value, arg2=default_value2):\n",
    "    # indented block of code\n",
    "```\n",
    "\n",
    "If there are mandatory arguments (that is, without default value) and optional arguments, then the latter have to follow the former.\n",
    "\n",
    "## Keyword Arguments\n",
    "\n",
    "If there are several optional arguments and only some shall be passed to the function, they can be provided as keyword arguments. The order of keyword arguments does not matter when calling a function.\n",
    "```python\n",
    "def my_function(arg1=default1, arg2=default2, arg3=default3):\n",
    "    # indented block of code\n",
    "\n",
    "my_function(arg3=12345, arg2=54321)\n",
    "```\n",
    "\n",
    "Keyword arguments are very common in Python, since they increase readability when calling functions with many arguments.\n",
    "\n",
    "## Arbitrary Number of Positional Arguments\n",
    "\n",
    "If we need a function which can take an arbitrary number of arguments, we may use the following snytax:\n",
    "```python\n",
    "def my_function(arg1, arg2, *other_args):\n",
    "    # indented block of code\n",
    "```\n",
    "\n",
    "Then `other_args` contains a tuple of all arguments passed to the function, but without `arg1` and `arg2`.\n",
    "\n",
    "## Arbitrary Number of Keyword Arguments\n",
    "\n",
    "If we need a function which can take an arbitrary number of keyword arguments, we may use the following snytax:\n",
    "```python\n",
    "def my_function(arg1, arg2, kwarg1=default1, kwarg2=default2, **other_kwargs):\n",
    "    # indented block of code\n",
    "```\n",
    "\n",
    "Then `other_kwargs` contains a dictionary of all keyword arguments passed to the function, but without `kwarg1` and `kwarg2`.\n",
    "\n",
    "## Argument Unpacking\n",
    "\n",
    "If we have a list or tuple and all items shall be passed as single arguments to a function, then we should use argument unpacking:\n",
    "```python\n",
    "my_list = [4, 3, 1]\n",
    "some_function(*my_list)\n",
    "some_function(my_list[0], my_list[1], my_list[2])\n",
    "```\n",
    "\n",
    "Both calls to `some_function` are equivalent.\n",
    "\n",
    "Same works with keyword arguments and dictionaries, where keys are argument names and values are values to be passed to the function.\n",
    "```python\n",
    "my_dict = {'kwarg1': 3, 'kwarg2': 5, 'kwarg3': 100}\n",
    "some_function(**my_dict)\n",
    "```\n",
    "\n",
    "## Memory Management\n",
    "\n",
    "### Passing Mutable Objects\n",
    "\n",
    "Python never copies objects passed to a function. Instead, the argument names in a function definition are tied to the objects, whose names are given in the function call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e05f61d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def some_function(arg1, arg2):\n",
    "    print(id(arg1), id(arg2))\n",
    "    \n",
    "a = 5\n",
    "b = 'some string'\n",
    "\n",
    "print(id(a), id(b))\n",
    "\n",
    "some_function(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb4731c7",
   "metadata": {},
   "source": [
    "Here we have to take care: if we pass mutable objects to a function, then the function may modify these objects!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40a25144",
   "metadata": {},
   "outputs": [],
   "source": [
    "def clear_list(l):\n",
    "    for k in range(0, len(l)):\n",
    "        l[k] = 0\n",
    "        \n",
    "my_list = [2, 5, 3]\n",
    "\n",
    "clear_list(my_list)\n",
    "\n",
    "print(my_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0ac35bfc",
   "metadata": {},
   "source": [
    "Always look up a function's documentation if you have to pass mutable objects to a function. If the function modifies an object, this fact should be provided in the documentation. For instance, several functions of the [OpenCV](https://opencv.org) library, which we'll use later on, modify their arguments without proper documentation.\n",
    "\n",
    "### Mutable Default Values\n",
    "\n",
    "A similar issue arises if we use mutable objects as default values for optional arguments. The name of an optional argument is tied to the object only once during execution (at time of function definition). If this object gets modified, then the default value changes for subsequent function calls."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "defb60ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "def append42(l = []):\n",
    "    l. append(42)\n",
    "    print(l)\n",
    "    \n",
    "append42()\n",
    "append42([1, 2, 3])\n",
    "append42()\n",
    "append42()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d91d05bc",
   "metadata": {},
   "source": [
    "To prevent such problems use a construction similar to the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a356e32",
   "metadata": {},
   "outputs": [],
   "source": [
    "def append42(l = None):\n",
    "    if l == None:\n",
    "        l = []\n",
    "    l. append(42)\n",
    "    print(l)\n",
    "    \n",
    "append42()\n",
    "append42([1, 2, 3])\n",
    "append42()\n",
    "append42()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe93ee98",
   "metadata": {},
   "source": [
    "## Restricting Argument Ppassing\n",
    "\n",
    "We may restrict a function's arguments to one of the following types:\n",
    "* positional only,\n",
    "* positional and keyword,\n",
    "* keyword only.\n",
    "\n",
    "For this purpose we have to add `/` and `*` to the argument list in a function's definition:\n",
    "```python\n",
    "def my_function(pos1, pos2, /, poskw1, poskw2, *, kw1, kw2):\n",
    "    # indented block of code\n",
    "```\n",
    "In a call to the function the first group of arguments has to be passed without keyword, the second group may be passed with or without keyword, and the third group has to be passed by keyword.\n",
    "\n",
    "The reason for existence of this technique is quite involved and, presumably, we won't need this feature. But we should know it to understand code written by others.\n",
    "\n",
    "## Functions in Python's Documentation\n",
    "\n",
    "Flexibility of argument passing makes it hard to clearly document which variants a library function accepts. Python's documentation uses a special syntax to state type and number of arguments as well as default values of a function.\n",
    "\n",
    "Example: The `glob` module's `glob` function (see [](python:accessing-data:file-io)) is shown in [Python's documentation](https://docs.python.org/3/library/glob.html#glob.glob) as follows:\n",
    "```\n",
    "glob.glob(pathname, *, root_dir=None, dir_fd=None, recursive=False)\n",
    "```\n",
    "We see:\n",
    "* `pathname` is the only positional argument.\n",
    "* There are three arguments which have to be passed by keyword.\n",
    "* `pathname` is mandatory, whereas the other arguments have default values.\n",
    "\n",
    "Another example: The built-in [`input`](https://docs.python.org/3/library/functions.html#input) function.\n",
    "```\n",
    "input([prompt])\n",
    "```\n",
    "* There is only one argument.\n",
    "* The argument is optional (indicated by `[...]`), that is, calling without any arguments is okay.\n",
    "\n",
    "One more: The built-in [`print`](https://docs.python.org/3/library/functions.html#print) function.\n",
    "```\n",
    "print(*objects, sep=' ', end='\\n', file=sys.stdout, flush=False)\n",
    "```\n",
    "* `print` accepts an arbitrary number of positional arguments.\n",
    "* `sep`, `end`, `file`, `flush` have to be passed by keyword (because they follow a `*` argument).\n",
    "* The four keyword arguments are optional (have default values)."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
