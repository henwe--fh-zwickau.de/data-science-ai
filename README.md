# Data Science and Artificial Intelligence for Undergraduates

This project is an executable book on data science and artificial intelligence built with [Jupyter Book](https://jupyterbook.org). It aims at undergraduates and provides material for a two-year data science course with emphasis on machine learning algorithms.

## Usage

The [official HTML rendering of the book](https://www.fh-zwickau.de/~jef19jdw/data-science-ai) is hosted at the project maintainer's personal website.

This repository contains the source code from which the book is built. For general information on rendering and output formats see [Jupyter Book website](https://jupyterbook.org).

To generate HTML rendering from source run
```
jb build .
```
in the source directory. For PDF rendering via LaTeX run
```
jb build . --builder pdflatex
```

See also [Hosting on a GitLab instance](#hosting-on-a-gitlab-instance) and [Non-Standard Configuration of Jupyter Book](#non-standard-configuration-of-jupyter-book) below.

## Support

Support is provided via GitLab issues for members of Saxon universities. People without suitable GitLab account may write an email to `jens.flemming@fh-zwickau.de` to get support.

## Contributing

At the moment the book is more or less a one-man project. Feel free to send bug reports, typos and so on to the author, see [Support](#support) above.

## License

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0)](http://creativecommons.org/licenses/by-sa/4.0/).

## Project Status

The project started in May 2022. A first complete version will be available in mid 2024.

At the moment the project is in early testing of the tool chain and contains no useful data science related content.

## Hosting on a GitLab Instance

Jupyter Book 0.13.0 only supports remote Git repositories hosted at [github.com](https://github.com). To use a self-hosted GitLab instance some tweaking may be required depending on which features you want to use. Three Python packages have to modified:
* `pydata-sphinx-theme` (version 0.8.1)
* `sphinx-book-theme` (version 0.3.2)
* `sphinx-thebe` (version 0.1.2)

### Repository Button

The repository button works out of the box with GitLab. But GitHub's icon is shown. To get GitLab's icon do the following.

Locate the install location of `sphinx_book_theme` Python package. If you installed Jupyter Book via conda it's something like `~/anaconda3/envs/env_name/lib/python3.10/site-packages/sphinx_book_theme`. Open `sphinx_book_theme/header_buttons/__init__.py` in a text editor and replace the two occurrences of `fa-github` by `fa-gitlab`. 

### Open Issue Button

The open issue button works with GitLab, at least in principle. But issue title and description aren't prefilled. To enable prefilling open `sphinx_book_theme/header_buttons/__init__.py` (see above) and replace `title=Issue` by `issue[title]=Issue` as well as `body` by `issue[description]`.

### Suggest Edit Button

The suggest edit button does not work because the link goes to github.com, not to the specified GitLab instance. To fix this, two files have to be modified:

In `sphinx_book_theme/header_buttons/__init__.py` (see above where to find `sphinx_book_theme` directory) go to
```python
context.update(
    {
        "github_user": org,
        "github_repo": repo,
        "github_version": branch,
        "doc_path": relpath,
    }
```
and replace `github` by `gitlab` three times.

In `pydata_sphinx_theme/__init__.py` (`pydata_sphinx_theme` should be in the same directory as `sphinx_book_theme`) go to
```python
default_provider_urls = {
    "bitbucket_url": "https://bitbucket.org",
    "github_url": "https://github.com",
    "gitlab_url": "https://gitlab.com",
}
```
and replace `gitlab.com` by the address of your GitLab instance.

### Binder and JupyterHub Buttons

Launching in Binder or JupyterHub does not work due to incorrect URLs. Open `sphinx_book_theme/header_buttons/launch.py` (see above where to find `sphinx_book_theme` directory) and the import
```python
from urllib.parse import quote as url_quote
```
to the file. Then go to `def _split_repo_url(url):` and replace the function by
```python
def _split_repo_url(url):
    """Split a repository URL into an org / repo combination."""
    end = url.split("your-gitlab.com/")[-1]
    org, repo = end.split("/")[:2]
    return org, repo
```
Find
```python
        url = (
            f"{binderhub_url}/v2/gh/{org}/{repo}/{branch}?"
            f"urlpath={ui_pre}/{path_rel_repo}"
        )
```
and replace it by
```python
        repo_url_quoted = url_quote(repo_url, safe='')
        url = (
            f"{binderhub_url}/v2/git/{repo_url_quoted}/{branch}?"
            f"urlpath={ui_pre}/{path_rel_repo}"
        )
```
Then find
```python
        url = (
            f"{jupyterhub_url}/hub/user-redirect/git-pull?"
            f"repo={repo_url}&urlpath={ui_pre}/{repo}/{path_rel_repo}&branch={branch}"
        )
```
and replace it by
```python
        repo_url_quoted = url_quote(repo_url, safe='')
        path_quoted = url_quote(f"{ui_pre}/{repo}/{path_rel_repo}", safe='')
        url = (
            f"{jupyterhub_url}/hub/user-redirect/git-pull?"
            f"repo={repo_url_quoted}&urlpath={path_quoted}&branch={branch}"
        )
```

### Thebe Button

Thebe button does not work. Open `sphinx_thebe/__init__.py` (`sphinx_thebe` should be in the same directory as `sphinx_book_theme`, see above) and go to `def _split_repo_url(url):`. Replace the function by
```python
def _split_repo_url(url):
    """Split a repository URL into an org / repo combination."""
    end = url.split("your-gitlab.com/")[-1]
    org, repo = end.split("/")[:2]
    return org, repo
```
Then search for
```
        binderOptions: {{
            repo: "{org}/{repo}",
            ref: "{branch}",
        }},
```
and replace it by
```
        binderOptions: {{
            repo: "{repo_url}",
            ref: "{branch}",
            binderUrl: "https://mybinder.org",
            repoProvider: "git",
        }},
```

## Non-Standard Configuration of Jupyter Book

The [official HTML rendering of the book](https://www.fh-zwickau.de/~jef19jdw/data-science-ai) shows some features not directly supported by Jupyter Book. They require modifications of the source code of Jupyter Book and related Python packages. If you want to use such non-standard features you should create a separate Python environment for editing the book.

### Custom Launch Button Labels

Locate the install location of `sphinx_book_theme` Python package. If you installed Jupyter Book via conda it's something like `~/anaconda3/envs/env_name/lib/python3.10/site-packages/sphinx_book_theme`. Open `sphinx_book_theme/header_buttons/launch.py` in a text editor and go to `        launch_buttons_list.append(`. Subsequent lines contain label information. Each launch button has such a block in the file.

### Notebooks for Launching, MyST Markdown for Editing

Launch and edit buttons use the same Git repository. There is no config option to provide different repositories for launching with Binder/JupyterHub and editing in GitLab. Notebooks are more common for launching, but MyST markdown in combination with [Jupytext](https://github.com/mwouts/jupytext) is much more comfortable for editing than directly working with notebooks. Jupytext allows to keep notebooks in one directory and corresponding markdown files in another (with synchronization in both directions). Thus, it would be nice to launch from the notebook directory, but edit the markdown files.

To get this working open `pydata_sphinx_theme/__init__.py` (`pydata_sphinx_theme` should be in the same directory as `sphinx_book_theme`) and replace
```python
file_name = f"{pagename}{context['page_source_suffix']}"
```
by
```python
file_name = f"{pagename}{context['page_source_suffix']}".replace('content', 'content_md').replace('.ipynb', '.md')
```
Here `content` directory contains the notebooks and `content_md` contains the markdown files. Then header of each markdown file should contain
```
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
```
to tell Jupytext to pair notebooks with markdown files in different directories.

### Different Branches for Launching and Editing

Depending on yours and your collaborator's workflow editing in a different branch than used for launching might be preferable. The official HTML rendering of this book currently does not use this feature, but we document it here.

In `pydata_sphinx_theme/__init__.py` (see above where to find it) replace
```python
"/-/edit/{{ gitlab_version }}/{{ doc_path }}{{ file_name }}"
```
by
```python
"/-/edit/your_branch_name_for_edits/{{ doc_path }}{{ file_name }}"
```
where `your_branch_name_for_edits` is the branch name to use for the edit button.

### No Book Title in Sidebar

The sidebar usually shows a logo and the book title. If the logo contains the title the additional text title is not needed. The remove the title from the sidebar open `sphinx_book_theme/theme/sphinx_book_theme/components/sidebar-logo.html` and enclose the line with `h1` tags in HTML comments `<!-- ... -->`.
