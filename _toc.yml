format: jb-book
root: content/preface
parts:
- caption: How to Use This Book
  chapters:
  - file: content/usage/executable-book
    sections:
    - file: content/usage/executable-book/read
    - file: content/usage/executable-book/execute
    - file: content/usage/executable-book/contribute
  - file: content/usage/guided-reading
    sections:
    - file: content/usage/guided-reading/data-science-1
    - file: content/usage/guided-reading/data-science-2
    - file: content/usage/guided-reading/data-science-3
    - file: content/usage/guided-reading/data-science-4
- caption: Warm-Up
  chapters:
  - file: content/warm-up/data-science-ai-ml
  - file: content/warm-up/python-jupyter
  - file: content/warm-up/computers-programming
- caption: Python for Data Science
  chapters:
  - file: content/python/crash-course
    sections:
    - file: content/python/crash-course/first-program
    - file: content/python/crash-course/building-blocks
    - file: content/python/crash-course/screen-io
    - file: content/python/crash-course/library-code
    - file: content/python/crash-course/everything-object
  - file: content/python/variables
    sections:
    - file: content/python/variables/names-objects
    - file: content/python/variables/types
    - file: content/python/variables/operators
    - file: content/python/variables/efficiency
  - file: content/python/lists-friends
    sections:
    - file: content/python/lists-friends/tuples
    - file: content/python/lists-friends/lists
    - file: content/python/lists-friends/dictionaries
    - file: content/python/lists-friends/iterable-objects
  - file: content/python/strings
    sections:
    - file: content/python/strings/basics
    - file: content/python/strings/special-characters
    - file: content/python/strings/formatting
  - file: content/python/accessing-data
    sections:
    - file: content/python/accessing-data/file-io
    - file: content/python/accessing-data/text-files
    - file: content/python/accessing-data/zip-files
    - file: content/python/accessing-data/csv-files
    - file: content/python/accessing-data/html-files
    - file: content/python/accessing-data/xml-files
    - file: content/python/accessing-data/web-access
  - file: content/python/functions
    sections:
    - file: content/python/functions/basics
    - file: content/python/functions/passing-arguments
    - file: content/python/functions/anonymous-functions
    - file: content/python/functions/function-method-objects
    - file: content/python/functions/recursion
  - file: content/python/modules-packages
  - file: content/python/error-handling-debugging-overview
  - file: content/python/inheritance
  - file: content/python/further-python-features
- caption: Exercises
  chapters:
  - file: content/exercises/computer-basics
  - file: content/exercises/python-programming
    sections:
    - file: content/exercises/python-programming/finding-errors
    - file: content/exercises/python-programming/basics
    - file: content/exercises/python-programming/more-basics
    - file: content/exercises/python-programming/variables-operators
    - file: content/exercises/python-programming/memory-management
    - file: content/exercises/python-programming/lists-friends
    - file: content/exercises/python-programming/strings
    - file: content/exercises/python-programming/file-access
    - file: content/exercises/python-programming/functions
    - file: content/exercises/python-programming/object-oriented-programming
- caption: Projects
  chapters:
  - file: content/projects/install-use-python
    sections:
    - file: content/projects/install-use-python/jupyterlab
    - file: content/projects/install-use-python/install-jupyter
    - file: content/projects/install-use-python/python-without-jupyter
    - file: content/projects/install-use-python/long-running
  - file: content/projects/python-programming
    sections:
    - file: content/projects/python-programming/simple-list-algorithms
    - file: content/projects/python-programming/geometric-objects
    - file: content/projects/python-programming/vector-multiplication
  - file: content/projects/weather
    sections:
    - file: content/projects/weather/dwd-open-data
    - file: content/projects/weather/getting-forecasts
  - file: content/projects/mnist
    sections:
    - file: content/projects/mnist/xmnist
  - file: content/projects/cafeteria
- caption: Mathematics
  chapters:
  - file: content/math/linear-algebra
    sections:
    - file: content/math/linear-algebra/vectors
- caption: Software Development
  chapters:
  - file: content/software-development/uml
  